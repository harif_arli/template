<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbClassroom;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'ห้องเรียน';
        $classroom   = TbClassroom::get();
        return view('pages.classroom.index', compact('page_title', 'classroom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มห้องเรียน';
        return view('pages.classroom.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        TbClassroom::create($requestData);
        return redirect()->route('classroom.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขห้องเรียน';
        $classroom = TbClassroom::find($id);
        return view('pages.classroom.edit',compact('page_title','classroom','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classroom = TbClassroom::find($id);
        $classroom->clr_ID = $request->input('clr_ID');
        $classroom->clr_Name = $request->input('clr_Name');
        $classroom->clr_Type = $request->input('clr_Type');
        $classroom->clr_Seat = $request->input('clr_Seat');
        $classroom->save();
        return redirect()->route('classroom.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classroom = TbClassroom::find($id);
        $classroom->delete();
        return redirect()->route('classroom.index')->with('success', 'ลบข้อมูลเรียบร้อย');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbMainCourse;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'หลักสูตร';
        $course   = TbMainCourse::get();
        return view('pages.course.index', compact('page_title', 'course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มหลักสูตร';
        return view('pages.course.create', compact('page_title',));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $requestData = $request->all();
        TbMainCourse::create($requestData);
        return redirect()->route('course.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขหลักสูตร';
        $course = TbMainCourse::find($id);
        return view('pages.course.edit',compact('page_title','course','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = TbMainCourse::find($id);
        $course->mc_id = $request->input('mc_id');
        $course->mc_name = $request->input('mc_name');
        $course->mc_year = $request->input('mc_year');
        $course->save();
        return redirect()->route('course.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = TbMainCourse::find($id);
        $course->delete();
        return redirect()->route('course.index')->with('success', 'ลบข้อมูลเรียบร้อย');
    }
}

<?php

namespace App\Http\Controllers;
use App\TbCourse;
use App\TbFaculty;
use App\TbMainCourse;
use App\TbMajor;
use App\TbSubject;
use App\TbGroupSubj;
use App\TbMajorType;
use App\TbSubjGroup;
use Illuminate\Http\Request;

class EducourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'แผนการเรียน';
        $edu = TbCourse::all();
        return view('pages.educourse.index', compact('page_title','edu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มการสอน';
        $faculty = TbFaculty::all();
        $mainCourse = TbMainCourse::all();
        $major = TbMajor::all();
        $subject = TbSubject::all();
        $groupSubj = TbGroupSubj::all();
        $subjGroupe = TbSubjGroup::all();
        return view('pages.educourse.create', compact('page_title','faculty','mainCourse','major','subject','groupSubj','subjGroupe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        TbCourse::create($requestData);
        return redirect()->route('educourse.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'เพิ่มการสอน';
        $edu = TbCourse::find($id);
        $faculty = TbFaculty::all();
        $mainCourse = TbMainCourse::all();
        $major = TbMajor::all();
        $subject = TbSubject::all();
        $groupSubj = TbGroupSubj::all();
        $subjGroupe = TbSubjGroup::all();
        return view('pages.educourse.edit', compact('page_title','faculty','mainCourse','major','subject','groupSubj','subjGroupe','edu','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edu = TbCourse::find($id);
        $edu->fac_id = $request->input('fac_id');
        $edu->major_id = $request->input('major_id');
        $edu->mc_id = $request->input('mc_id');
        $edu->mc_year = $request->input('mc_year');
        $edu->academic_y = $request->input('academic_y');
        $edu->term = $request->input('term');
        $edu->subj_id = $request->input('subj_id');
        $edu->group_subj_id = $request->input('group_subj_id');
        $edu->subj_group_id = $request->input('subj_group_id');
        $edu->save();
        return redirect()->route('educourse.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $edu = TbCourse::find($id);
        $edu->delete();
        return redirect()->route('educourse.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

<?php

namespace App\Http\Controllers;

use App\District;
use App\Province;
use App\SubDistrict;
use App\TbEmpt;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'เจ้าหน้าที่';
        $employee = TbEmpt::get();
        return view('pages.employee.index', compact('page_title', 'employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มข้อมูลเจ้าหน้าที่';
        $province = Province::all();
        return view('pages.employee.create', compact('page_title', 'province'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploade = new UploadeFileController();

        if (!empty($request->empt_img)) {
            $request->empt_img = $uploade->uploadImage(
                $request->empt_img,
                'employee',
                Str::random(5)
            );
        }
        $requestData = $request->all();
        $requestData['empt_img'] = $request->empt_img;
        TbEmpt::create($requestData);
        return redirect()->route('employee.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $page_title = 'Update employee';
        $employee = TbEmpt::find($id);
        $province = Province::all();
        $province_names = Province::all();
        return view('pages.employee.edit', compact('page_title', 'employee', 'province', 'id'));
    }

    public function update(Request $request, $id)
    {
        $uploade = new UploadeFileController();

        if (!empty($request->empt_img)) {
            $request->empt_img = $uploade->uploadImage(
                $request->empt_img,
                'employee',
                Str::random(5)
            );
        }
        $employee = TbEmpt::find($id);
        $employee->empt_ID = $request->get('empt_ID');
        $employee->empt_title = $request->get('empt_title');
        $employee->empt_Name_th = $request->get('empt_Name_th');
        $employee->empt_Lname_th = $request->get('empt_Lname_th');
        $employee->empt_Name_en = $request->get('empt_Name_en');
        $employee->empt_Lname_en = $request->get('empt_Lname_en');
        $employee->empt_p_ID = $request->get('empt_p_ID');
        $employee->empt_tel = $request->get('empt_tel');
        $employee->email = $request->get('email');
        $employee->religion = $request->get('religion');
        $employee->blood_type = $request->get('blood_type');
        $employee->disability = $request->get('disability');
        $employee->addr_birthplace = $request->get('addr_birthplace');
        $employee->addr_b_t = $request->get('addr_b_t');
        $employee->addr_b_city = $request->get('addr_b_city');
        $employee->addr_b_prov = $request->get('addr_b_prov');
        $employee->addr_b_post = $request->get('addr_b_post');
        $employee->addr_cerrent = $request->get('addr_cerrent');
        $employee->addr_c_t = $request->get('addr_c_t');
        $employee->addr_c_city = $request->get('addr_c_city');
        $employee->addr_c_prov = $request->get('addr_c_prov');
        $employee->addr_c_post = $request->get('addr_c_post');
        $employee->birthday = $request->get('birthday');
        $employee->work_date = $request->get('work_date');
        $employee->date_permit = $request->get('date_permit');
        $employee->retire_date = $request->get('retire_date');
        $employee->expertise = $request->get('expertise');
        $employee->save();
        return redirect()->route('employee.index')->with('success', 'อัพเดตเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = TbEmpt::find($id);
        $employee->delete();
        return redirect()->route('employee.index')->with('success', 'ลบข้อมูลเรียบร้อย');
    }

    public function district(Request $request)
    {
        if ($request->ajax()) {
            $query = District::where('ref_province_id', $request->province_id)->get()->toArray();
            return response()->json($query);
        }
    }

    public function subdistrict(Request $request)
    {
        if ($request->ajax()) {
            $query = SubDistrict::where('ref_district_id', $request->district_id)->get()->toArray();
            return response()->json($query);
        }
    }
}

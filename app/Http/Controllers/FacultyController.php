<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbFaculty;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'คณะ';
        $faculty   = TbFaculty::get();
        return view('pages.faculty.index', compact('page_title', 'faculty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มคณะ';
        return view('pages.faculty.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        TbFaculty::create($requestData);
        return redirect()->route('faculty.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขคณะ';
        $faculty = TbFaculty::find($id);
        return view('pages.faculty.edit',compact('page_title','faculty','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faculty = TbFaculty::find($id);
        $faculty->fac_id = $request->input('fac_id');
        $faculty->fac_name = $request->input('fac_name');
        $faculty->save();
        return redirect()->route('faculty.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faculty = TbFaculty::find($id);
        $faculty->delete();
        return redirect()->route('faculty.index')->with('success', 'ลบข้อมูลเรียบร้อย');
    }
}

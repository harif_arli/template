<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbGroupSubj;
use App\TbSubjGroup;
class GroupsubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'กลุ่มวิชา';
        $groupsubject = TbGroupSubj::get();
        return view('pages.groupsubject.index', compact('page_title','groupsubject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มกลุ่มวิชา';
        $subjectgroup = TbSubjGroup::get();
        return view('pages.groupsubject.create', compact('page_title','subjectgroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $requestData = $request->all();
        TbGroupSubj::create($requestData);
        return redirect()->route('groupsubject.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขกลุ่มวิชา';
        $subjectgroup = TbSubjGroup::get();
        $groupsubject = TbGroupSubj::find($id);
        return view('pages.groupsubject.edit',compact('page_title','groupsubject','subjectgroup','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupsubject = TbGroupSubj::find($id);
        $groupsubject->group_subj_id = $request->input('group_subj_id');
        $groupsubject->group_subj_name = $request->input('group_subj_name');
        $groupsubject->subj_group_id = $request->input('group_subj_id');
        $groupsubject->save();
        return redirect()->route('groupsubject.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupsubject = TbGroupSubj::find($id);
        $groupsubject->delete();
        return redirect()->route('groupsubject.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

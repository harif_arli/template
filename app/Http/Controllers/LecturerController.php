<?php

namespace App\Http\Controllers;
use App\TbLecturer;
use App\TbTeache;
use App\TbSubject;
use Illuminate\Http\Request;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'กำหนดผู้สอน';
        $lecturer = TbLecturer::all();
        return view('pages.lecturer.index', compact('page_title','lecturer'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'หน้ารวมผู้สอน';
        $teache = TbTeache::get();
        $subject = TbSubject::get();
        return view('pages.lecturer.create', compact('page_title','teache','subject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        TbLecturer::create($requestData);
        return redirect()->route('lecturer.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขผู้สอน';
        $lecturer = TbLecturer::find($id);
        $teache = TbTeache::get();
        $subject = TbSubject::get();
        return view('pages.lecturer.edit',compact('page_title','lecturer','teache','subject','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lecturer = TbLecturer::find($id);
        $lecturer->t_ID = $request->get('t_ID');
        $lecturer->subj_id = $request->get('subj_id');
        $lecturer->session = $request->get('session');
        $lecturer->term = $request->get('term');
        $lecturer->year = $request->get('year');
        $lecturer->seat = $request->get('seat');
        $lecturer->save();
        return redirect()->route('lecturer.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lecturer = TbLecturer::find($id);
        $lecturer->delete();
        return redirect()->route('lecturer.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbMajor;
use App\TbMajorType;

use function PHPSTORM_META\type;

class MajorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'สาขาวิชา';
        $major = TbMajor::get();
        return view('pages.major.index', compact('page_title','major'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มสาขาวิชา';
        $majortype = TbMajorType::all();
        return view('pages.Major.create', compact('page_title','majortype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $requestData = $request->all();
        TbMajor::create($requestData);
        return redirect()->route('major.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขข้อมูลสาขา';
        $major = TbMajor::find($id);
        $majortype = TbMajorType::all();
        return view('pages.Major.edit',compact('page_title','major','majortype','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $major = TbMajor::find($id);
        $major->major_id = $request->get('major_id');
        $major->major_name = $request->get('major_name');
        $major->major = $request->get('major');
        $major->major_type_id = $request->get('major_type_id');
        $major->save();
        return redirect()->route('major.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $major = TbMajor::find($id);
        $major->delete();
        return redirect()->route('major.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

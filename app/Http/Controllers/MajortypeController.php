<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbMajorType;

class MajortypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $page_title = 'ประเภทวิชา';
         $majorType = TbMajorType::get();
         return view('pages.majortype.index', compact('page_title','majorType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มประเภทวิชา';
        return view('pages.majortype.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        TbMajorType::create($requestData);
        return redirect()->route('majortype.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขประเภทวิชา';
        $majorType = TbMajorType::find($id);
        return view('pages.majortype.edit',compact('page_title','majorType','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $majorType = TbMajorType::find($id);
        $majorType->major_type_id = $request->get('major_type_id');
        $majorType->major_type_name = $request->get('major_type_name');
        $majorType->save();
        return redirect()->route('majortype.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $majorType = TbMajorType::find($id);
        $majorType->delete();
        return redirect()->route('majortype.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

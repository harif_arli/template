<?php

namespace App\Http\Controllers;

use App\District;
use App\SubDistrict;
use Illuminate\Http\Request;


class ProviceController extends Controller
{
    public function district(Request $request)
    {
        if ($request->ajax()) {
            $query = District::where('ref_province_id', $request->province_id)->get()->toArray();
            return response()->json($query);
        }
    }

    public function subdistrict(Request $request)
    {
        if ($request->ajax()) {
            $query = SubDistrict::where('ref_district_id', $request->district_id)->get()->toArray();
            return response()->json($query);
        }
    }

    public function poscode(Request $request)
    {
        if ($request->ajax()) {
            $query = SubDistrict::where('sub_district_id', $request->subdistrict_id)->get();
            return $query;
        }
    }

    public function district1(Request $request)
    {
        if ($request->ajax()) {
            $query = District::where('ref_province_id', $request->province_id)->get()->toArray();
            return response()->json($query);
        }
    }

    public function subdistrict1(Request $request)
    {
        if ($request->ajax()) {
            $query = SubDistrict::where('ref_district_id', $request->district_id)->get()->toArray();
            return response()->json($query);
        }
    }

}

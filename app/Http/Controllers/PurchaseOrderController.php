<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Branch;
use App\Company;
use App\Department;
use App\PurchaseRequest;

class PurchaseOrderController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $page_title = 'Purchase Order';
    return view('pages.PurchaseOrder.index', compact('page_title'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $page_title = 'Create Purchase Order';
    $company = Company::get();
    $branch = Branch::get();
    $department = Department::get();
    return view('pages.PurchaseOrder.create', compact('page_title', 'company', 'branch', 'department'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $request->validate([
      'title' => 'required',
      'purpose' => 'required',
      'department_id' => 'required',
      'branch_id' => 'required',
      'company_id' => 'required',
      'product' => 'required',
      'product_img' => 'required|image|max:2048',
    ]);

    // upload to firebaseStorage
    $storage = app('firebase.storage');
    $bucket = $storage->getBucket();
    $originalFileName = $request->file('product_img')->getClientOriginalName();
    $ext = File::extension($originalFileName);
    $uniqFileName = uniqid() . '.' . $ext;
    $bucket->upload(
      file_get_contents($request->file('product_img')),
      [
        'name' => $uniqFileName
      ]
    );

    $requestData = $request->all();
    $requestData['product_img'] = $uniqFileName;
    PurchaseRequest::create($requestData);
    return redirect()->route('purchase-order.index')->with('success', 'Purchase Request created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbSubject;
use App\TbGroupSubj;
class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'รายวิชา';
        $subject = TbSubject::get();
        return view('pages.subject.index', compact('page_title','subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มรายวิชา';
        $groupsubject = TbGroupSubj::get();
        return view('pages.subject.create', compact('page_title','groupsubject'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        TbSubject::create($requestData);
        return redirect()->route('subject.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขข้อมูลรายวิชา';
        $subject = TbSubject::find($id);
        $groupsubject = TbGroupSubj::get();
        return view('pages.Subject.edit',compact('page_title','subject','groupsubject','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = TbSubject::find($id);
        $subject->subj_id = $request->get('subj_id');
        $subject->subj_name_th = $request->get('subj_name_th');
        $subject->subj_name_en = $request->get('subj_name_en');
        $subject->cr_lec = $request->get('cr_lec');
        $subject->cr_lab = $request->get('cr_lab');
        $subject->credit = $request->get('credit');
        $subject->detail = $request->get('detail');
        $subject->group_subj_id = $request->get('group_subj_id');
        $subject->save();
        return redirect()->route('subject.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = TbSubject::find($id);
        $subject->delete();
        return redirect()->route('subject.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TbSubjGroup;
class SubjectgroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'หมวดวิชา';
        $subjectgroup = TbSubjGroup::get();
        return view('pages.subjectgroup.index', compact('page_title','subjectgroup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มหมวดวิชา';
        return view('pages.subjectgroup.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $requestData = $request->all();
        TbSubjGroup::create($requestData);
        return redirect()->route('subjectgroup.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขหมวดวิชา';
        $subjectgroup = TbSubjGroup::find($id);
        return view('pages.Subjectgroup.edit',compact('page_title','subjectgroup','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subjectgroup = TbSubjGroup::find($id);
        $subjectgroup->subj_group_id = $request->get('subj_group_id');
        $subjectgroup->subj_group_name = $request->get('subj_group_name');
        $subjectgroup->save();
        return redirect()->route('subjectgroup.index')->with('success','แก้ใขเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subjectgroup = TbSubjGroup::find($id);
        $subjectgroup->delete();
        return redirect()->route('subjectgroup.index')->with('success','ลบข้อมูลเรียบร้อย');
    }
}

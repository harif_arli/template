<?php

namespace App\Http\Controllers;

use App\TbTeache;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'ข้อมูลอาจารย์';
        $teacher = TbTeache::get();
        return view('pages.Teacher.index', compact('page_title', 'teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'เพิ่มข้อมูลอาจารย์';
        return view('pages.Teacher.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uploade = new UploadeFileController();

        if (!empty($request->t_img)) {
            $request->t_img = $uploade->uploadImage(
                $request->t_img,
                'teacher',
                Str::random(5)
            );
        }
        $requestData = $request->all();
        $requestData['t_img'] = $request->t_img;
        TbTeache::create($requestData);
        return redirect()->route('teacher.index')->with('success', 'เพิ่มข้อมูลสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'แก้ใขข้อมูลอาจารย์';
        $teacher = TbTeache::find($id);
        return view('pages.Teacher.edit', compact('page_title', 'teacher', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = TbTeache::find($id);
        $teacher->t_ID = $request->get('t_ID');
        $teacher->t_title = $request->get('t_title');
        $teacher->t_Name_th = $request->get('t_Name_th');
        $teacher->t_Lname_th = $request->get('t_Lname_th');
        $teacher->t_Name_en = $request->get('t_Name_en');
        $teacher->t_Lname_en = $request->get('t_Lname_en');
        $teacher->t_p_ID = $request->get('t_p_ID');
        $teacher->t_tel = $request->get('t_tel');
        $teacher->email = $request->get('email');
        $teacher->blood_type = $request->get('blood_type');
        $teacher->disability = $request->get('disability');
        $teacher->addr_birthplace = $request->get('addr_birthplace');
        $teacher->addr_b_t = $request->get('addr_b_t');
        $teacher->addr_b_city = $request->get('addr_b_city');
        $teacher->addr_b_prov = $request->get('addr_b_prov');
        $teacher->addr_b_post = $request->get('addr_b_post');
        $teacher->addr_cerrent = $request->get('addr_cerrent');
        $teacher->addr_c_t = $request->get('addr_c_t');
        $teacher->addr_c_city = $request->get('addr_c_city');
        $teacher->addr_c_prov = $request->get('addr_c_prov');
        $teacher->addr_c_post = $request->get('addr_c_post');
        $teacher->birthday = $request->get('birthday');
        $teacher->work_date = $request->get('work_date');
        $teacher->date_permit = $request->get('date_permit');
        $teacher->retire_date = $request->get('retire_date');
        $teacher->major_id = $request->get('major_id');
        $teacher->subj_group_id = $request->get('subj_group_id');
        $teacher->mate = $request->get('mate');
        $teacher->father = $request->get('father');
        $teacher->mother = $request->get('mother');
        $teacher->position = $request->get('position');
        $teacher->position_exec = $request->get('position_exec');
        $teacher->degree = $request->get('degree');
        $teacher->university = $request->get('university');
        $teacher->year = $request->get('year');
        $teacher->save();
        return redirect()->route('teacher.index')->with('success', 'อัพเดตเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = TbTeache::find($id);
        $teacher->delete();
        return redirect()->route('teacher.index')->with('success', 'ลบข้อมูลเรียบร้อย');
    }
}

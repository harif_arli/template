<?php
if (!function_exists('getImgPathFirebase')) {
  function getImgPathFirebase($img_name)
  {
    $img_path = 'https://firebasestorage.googleapis.com/v0/b/register-tech.appspot.com/o/' . $img_name . '?alt=media';
    return $img_path;
  }
}

if (!function_exists('sendLineNotification')) {
  function sendLineNotification($msg, $token)
  {
    $url     = 'https://notify-api.line.me/api/notify';
    $token   = $token;
    $headers = [
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token,
    ];
    $fields = "message=$msg";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);

    // var_dump($result);
    $result = json_decode($result, true);
  }
}

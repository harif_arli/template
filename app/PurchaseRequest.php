<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
  protected $table = 'purchase_request';
  protected $fillable = [
    'title', 'purpose', 'department_id', 'branch_id', 'company_id', 'product', 'product_img'
  ];
}

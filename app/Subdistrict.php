<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    protected $table      = 'subdistrict';
    protected $primaryKey = 'sub_district_id';
    protected $fillable   = ['postcode', 'sub_district_name','ref_district_id'];
}

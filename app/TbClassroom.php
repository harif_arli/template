<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbClassroom extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'clr_ID';
    protected $keyType = 'string';
    protected $table = 'tb_classrooms';
    protected $fillable = ['clr_ID','clr_Name','clr_Type','clr_Seat'];
}

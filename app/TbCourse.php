<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbCourse extends Model
{
    use SoftDeletes;
    protected $table = 'tb_courses';
    protected $fillable = ['fac_id','major_id','mc_id','mc_year','academic_y','term','subj_id','group_subj_id','subj_group_id'];

    public function faculty()
    {
     return $this->belongsTo(TbFaculty::class,'fac_id', 'fac_id');
    }

    public function major()
    {
     return $this->belongsTo(TbMajor::class,'major_id', 'major_id');
    }

    public function maincourse()
    {
     return $this->belongsTo(TbMainCourse::class,'mc_id', 'mc_id');
    }

    public function subject()
    {
     return $this->belongsTo(TbSubject::class,'subj_id', 'subj_id');
    }

    public function groupSubj()
    {
     return $this->belongsTo(TbGroupSubj::class,'group_subj_id', 'group_subj_id');
    }

    public function SubjGroup()
    {
     return $this->belongsTo(TbSubjGroup::class,'subj_group_id', 'subj_group_id');
    }
}

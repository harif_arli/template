<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbEmpt extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'empt_ID';
    protected $keyType = 'string';
    protected $table    = 'tb_empts';
    protected $fillable = ['empt_ID', 'empt_title', 'empt_Name_th', 'empt_Lname_th', 'empt_Name_en', 'empt_Lname_en', 'empt_img', 'empt_p_ID',
        'empt_tel', 'email', 'religion', 'blood_type', 'disability', 'addr_birthplace', 'addr_b_t', 'addr_b_city', 'addr_b_prov', 'addr_b_post', 'addr_cerrent', 'addr_c_t', 'addr_c_city',
        'addr_c_prov', 'addr_c_post', 'birthday', 'work_date', 'date_permit', 'retire_date', 'date_permit', 'retire_date', 'expertise',
    ];
}

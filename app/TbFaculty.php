<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbFaculty extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'fac_id';
    protected $keyType = 'string';
    protected $table = 'tb_faculties';
    protected $fillable = ['fac_id','fac_name'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbGroupSubj extends Model
{

    use SoftDeletes;

    protected $primaryKey = 'group_subj_id';
    protected $keyType = 'string';
    protected $table = 'tb_group_subjs';
    protected $fillable = ['group_subj_id','group_subj_name','subj_group_id'];

    public function subjectgroup()
    {
     return $this->belongsTo(TbSubjGroup::class,'subj_group_id', 'subj_group_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbLecturer extends Model
{
    use SoftDeletes;
    protected $table = 'tb_lecturers';
    protected $fillable = ['t_ID','subj_id','session','term','year','seat'];

    public function teacher()
    {
     return $this->belongsTo(TbTeache::class,'t_ID', 't_ID');
    }

    public function subject()
    {
     return $this->belongsTo(TbSubject::class,'subj_id', 'subj_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbMainCourse extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'mc_id';
    protected $keyType = 'string';
    protected $table = 'tb_main_courses';
    protected $fillable = ['mc_id','mc_name','mc_year'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbMajor extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'major_id';
    protected $keyType = 'string';
    protected $table = 'tb_majors';
    protected $fillable = ['major_id','major_name','major','major_type_id'];

    public function majortype()
 {
  return $this->belongsTo(TbMajorType::class,'major_type_id', 'major_type_id');
 }
}

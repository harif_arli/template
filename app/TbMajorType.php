<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbMajorType extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'major_type_id';
    protected $keyType = 'string';
    protected $table = 'tb_major_types';
    protected $fillable = ['major_type_id','major_type_name'];
}

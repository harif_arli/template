<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbStudent extends Model
{
    use SoftDeletes;

    protected $primaryKey = 's_ID';
    protected $keyType = 'string';
    protected $table = 'tb_students';
    protected $fillable = ['s_ID','s_title','s_Name_th','s_Lname_th','s_Name_en','s_Lname_en','s_img','s_p_ID','s_tel','nationality',
        'race','religion','blood_type','disability','addr_birthplace','addr_b_t','addr_b_city','addr_b_prov','addr_b_post',
        'addr_cerrent','addr_c_t','addr_c_city','addr_c_prov','addr_c_post','degree','majorID','year_edu','old_school_name','old_major',
        'old_degree','old_school_add','old_school_t','old_school_city','old_school_prov','old_school_post','father_pID',
        'father_title','father_name','father_lname','father_opt','father_income','father_tel','father_status','mother_pID',
        'mother_title','mother_name','mother_lname','mother_opt','mother_income','mother_tel','mother_status'];
}

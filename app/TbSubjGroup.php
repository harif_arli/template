<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbSubjGroup extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'subj_group_id';
    protected $keyType = 'string';
    protected $table = 'tb_subj_groups';
    protected $fillable = ['subj_group_id','subj_group_name'];
}

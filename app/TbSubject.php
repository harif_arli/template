<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbSubject extends Model
{
    protected $primaryKey = 'subj_id';
    protected $keyType = 'string';
    protected $table = 'tb_subjects';
    protected $fillable = ['subj_id','subj_name_th','subj_name_en','cr_lec','cr_lab','credit','detail','group_subj_id'];

    public function groupsubject()
 {
  return $this->belongsTo(TbGroupSubj::class,'group_subj_id', 'group_subj_id');
 }
}

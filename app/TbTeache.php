<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbTeache extends Model
{
    use SoftDeletes;

    protected $primaryKey = 't_ID';
    protected $keyType = 'string';
    protected $table = 'tb_teaches';
    protected $fillable = ['t_ID','t_title','t_Name_th','t_Lname_th','t_Name_en','t_Lname_en','t_img','t_p_ID',
    't_tel','email','religion','blood_type','disability','addr_birthplace','addr_birthplace', 'addr_b_t','addr_b_city','addr_b_prov','addr_b_post', 'addr_cerrent','addr_c_t','addr_c_city','addr_c_prov','addr_c_post','birthday','work_date','date_permit','retire_date','date_permit','retire_date','expertise','major_id','subj_group_id','date_permit',
    'retire_date','expertise','major_id','subj_group_id','mate','father','mother','position','position_exec','degree','university','year'];
}

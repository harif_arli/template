<?php
// Aside menu
return [
  'items' => [


     //Dashboard
    [
       'title' => 'Dashboard',
       'root' => true,
       'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
       'page' => '/',
       'new-tab' => false,
    ],

    [
        'title' => 'นักศึกษา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/student',
        'new-tab' => false,
    ],

    [
        'title' => 'อาจารย์',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/teacher',
        'new-tab' => false,
    ],

    [
        'title' => 'เจ้าหน้าที่',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/employee',
        'new-tab' => false,
    ],

    [
        'title' => 'ประเภทวิชา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/majortype',
        'new-tab' => false,
    ],

    [
        'title' => 'สาขาวิชา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/major',
        'new-tab' => false,
    ],

    [
        'title' => 'หมวดวิชา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/subjectgroup',
        'new-tab' => false,
    ],


    [
        'title' => 'กลุ่มวิชา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/groupsubject',
        'new-tab' => false,
    ],

    [
        'title' => 'รายวิชา',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/subject',
        'new-tab' => false,
    ],

    [
        'title' => 'คณะ',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/faculty',
        'new-tab' => false,
    ],

    [
        'title' => 'หลักสูตร',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/course',
        'new-tab' => false,
    ],

    [
        'title' => 'ห้องเรียน',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/classroom',
        'new-tab' => false,
    ],

    [
        'title' => 'กำหนดผู้สอน',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/lecturer',
        'new-tab' => false,
    ],

    [
        'title' => 'แผนการเรียน',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'page' => '/educourse',
        'new-tab' => false,
    ],

    [
        'title' => 'ประเมิน',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        //'page' => '',
        'new-tab' => false,
    ],

    // Dashboard
    // [
    //   'title' => 'Dashboard',
    //   'root' => true,
    //   'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
    //   'page' => '/',
    //   'new-tab' => false,
    // ],

    // // Purchase Requisition
    // [
    //   'section' => 'Purchase Requisition',
    // ],
    // [
    //   'title' => 'Purchase order',
    //   'icon' => 'media/svg/icons/Layout/Layout-4-blocks.svg',
    //   'page' => '/purchase-order',
    //   'new-tab' => false,
    // ],

    // // Assets
    // [
    //   'section' => 'Assets Management',
    // ],
    // [
    //   'title' => 'Assets',
    //   'icon' => 'media/svg/icons/Shopping/Barcode-read.svg',
    //   'page' => '/',
    //   'new-tab' => false,
    // ],
    // [
    //   'title' => 'Borrow assets',
    //   'icon' => 'media/svg/icons/Home/Library.svg',
    //   'page' => '/',
    //   'new-tab' => false,
    // ],

    // // Accounting
    // [
    //   'section' => 'Accounting',
    // ],
    // [
    //   'title' => 'PMO',
    //   'icon' => 'media/svg/icons/Layout/Layout-horizontal.svg',
    //   'page' => '/',
    //   'new-tab' => false,
    // ],

    // // Settings
    // [
    //   'section' => 'Settings',
    // ],
    // [
    //   'title' => 'นักศึกษา',
    //   'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
    //   'page' => '/student',
    //   'new-tab' => false,
    // ],
    // [
    //     'title' => 'Department',
    //     'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
    //     'page' => '/department',
    //     'new-tab' => false,
    // ],
    // [
    //     'title' => 'Branch',
    //     'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
    //     'page' => '/branch',
    //     'new-tab' => false,
    // ],
    // [
    //     'title' => 'Company',
    //     'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
    //     'page' => '/company',
    //     'new-tab' => false,
    // ],
  ]
];

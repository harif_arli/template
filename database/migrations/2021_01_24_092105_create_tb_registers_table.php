<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_registers', function (Blueprint $table) {
            $table->char('student_ID',10);
            $table->char('subj_id',10);
            $table->integer('session');
            $table->integer('term');
            $table->date('year',2);
            $table->timestamps();

            $table->primary([
                'student_ID',
                'subj_id',
                'session',
                'term',
                'year'
            ],'register');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_registers');
    }
}

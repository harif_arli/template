<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_students', function (Blueprint $table) {
            $table->char('s_ID', 10);
            $table->char('s_title', 30);
            $table->char('s_Name_th', 50);
            $table->char('s_Lname_th', 50);
            $table->char('s_Name_en', 50);
            $table->char('s_Lname_en', 50);
            $table->char('s_img', 255);
            $table->char('s_p_ID', 15);
            $table->char('s_tel', 50);
            $table->char('nationality', 28);
            $table->char('race', 29);
            $table->char('religion', 30);
            $table->char('blood_type', 2);
            $table->char('disability', 50);
            $table->char('addr_birthplace', 255);
            $table->char('addr_b_t', 255);
            $table->char('addr_b_city', 255);
            $table->char('addr_b_prov', 255);
            $table->char('addr_b_post', 255);
            $table->char('addr_cerrent', 255);
            $table->char('addr_c_t', 255);
            $table->char('addr_c_city', 255);
            $table->char('addr_c_prov', 255);
            $table->char('addr_c_post', 255);
            $table->char('degree', 255)->nullable();
            $table->char('majorID', 10)->nullable();
            $table->char('year_edu', 50)->nullable();
            $table->date('mc_year')->nullable();
            $table->char('old_school_name', 100)->nullable();
            $table->char('old_major', 255)->nullable();
            $table->char('old_degree', 255)->nullable();
            $table->char('old_school_add', 255)->nullable();
            $table->char('old_school_t', 255)->nullable();
            $table->char('old_school_city', 255)->nullable();
            $table->char('old_school_prov', 255)->nullable();
            $table->char('old_school_post', 255)->nullable();
            $table->char('father_pID', 15)->nullable();
            $table->char('father_title', 20)->nullable();
            $table->char('father_name', 100)->nullable();
            $table->char('father_lname', 100)->nullable();
            $table->char('father_opt', 100)->nullable();
            $table->char('father_income', 100)->nullable();
            $table->char('father_tel', 15)->nullable();
            $table->char('father_status', 50)->nullable();
            $table->char('mother_pID', 15)->nullable();
            $table->char('mother_title', 20)->nullable();
            $table->char('mother_name', 100)->nullable();
            $table->char('mother_lname', 100)->nullable();
            $table->char('mother_opt', 100)->nullable();
            $table->char('mother_income', 100)->nullable();
            $table->char('mother_tel', 15)->nullable();
            $table->char('mother_status', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->primary('s_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_students');
    }
}

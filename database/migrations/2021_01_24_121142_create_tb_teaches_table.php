<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbTeachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_teaches', function (Blueprint $table) {
            $table->char('t_ID', 10);
            $table->char('t_title', 30);
            $table->char('t_Name_th', 50);
            $table->char('t_Lname_th', 50);
            $table->char('t_Name_en', 50);
            $table->char('t_Lname_en', 50);
            $table->char('t_img', 255);
            $table->char('t_p_ID', 15);
            $table->char('t_tel', 50);
            $table->char('email', 255);
            $table->char('religion', 30);
            $table->char('blood_type', 2);
            $table->char('disability', 50);
            $table->char('addr_birthplace', 255);
            $table->char('addr_b_t', 255);
            $table->char('addr_b_city', 255);
            $table->char('addr_b_prov', 255);
            $table->char('addr_b_post', 255);
            $table->char('addr_cerrent', 255);
            $table->char('addr_c_t', 255);
            $table->char('addr_c_city', 255);
            $table->char('addr_c_prov', 255);
            $table->char('addr_c_post', 255);
            $table->date('birthday');
            $table->date('work_date')->nullable();
            $table->date('date_permit')->nullable();
            $table->date('retire_date')->nullable();
            $table->char('expertise', 255)->nullable();
            $table->char('major_id', 10)->nullable();
            $table->char('subj_group_id', 10)->nullable();
            $table->char('mate', 100)->nullable();
            $table->char('father', 100)->nullable();
            $table->char('mother', 100)->nullable();
            $table->char('position', 100)->nullable();
            $table->char('position_exec', 100)->nullable();
            $table->char('degree', 255)->nullable();
            $table->char('university', 100)->nullable();
            $table->char('year', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->primary('t_ID');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_teaches');
    }
}

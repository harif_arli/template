<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbEmptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_empts', function (Blueprint $table) {
            $table->char('empt_ID', 10);
            $table->char('empt_title', 30);
            $table->char('empt_Name_th', 50);
            $table->char('empt_Lname_th', 50);
            $table->char('empt_Name_en', 50);
            $table->char('empt_Lname_en', 50);
            $table->char('empt_img', 255);
            $table->char('empt_p_ID', 15);
            $table->char('empt_tel', 12);
            $table->char('email', 255);
            $table->char('religion', 30);
            $table->char('blood_type', 2);
            $table->char('disability', 50);
            $table->char('addr_birthplace', 255);
            $table->char('addr_b_t', 255);
            $table->char('addr_b_city', 255);
            $table->char('addr_b_prov', 255);
            $table->char('addr_b_post', 255);
            $table->char('addr_cerrent', 255);
            $table->char('addr_c_t', 255);
            $table->char('addr_c_city', 255);
            $table->char('addr_c_prov', 255);
            $table->char('addr_c_post', 255);
            $table->date('birthday');
            $table->date('work_date')->nullable();
            $table->date('date_permit')->nullable();
            $table->date('retire_date')->nullable();
            $table->char('expertise', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->primary('empt_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_empts');
    }
}

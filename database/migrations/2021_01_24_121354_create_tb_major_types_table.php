<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMajorTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_major_types', function (Blueprint $table) {
            $table->char('major_type_id',2);
            $table->char('major_type_name',255);
            $table->timestamps();
            $table->softDeletes();


            $table->primary('major_type_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_major_types');
    }
}

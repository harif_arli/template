<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMajorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_majors', function (Blueprint $table) {
            $table->char('major_id',2);
            $table->char('major_name',255);
            $table->string('major');
            $table->char('major_type_id',10);
            $table->timestamps();
            $table->softDeletes();


            $table->primary('major_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_majors');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbSubjGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_subj_groups', function (Blueprint $table) {
            $table->char('subj_group_id',2);
            $table->char('subj_group_name',255);
            $table->timestamps();
            $table->softDeletes();

            $table->primary('subj_group_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_subj_groups');
    }
}

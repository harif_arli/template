<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbGroupSubjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_group_subjs', function (Blueprint $table) {
            $table->char('group_subj_id',2);
            $table->char('group_subj_name', 100);
            $table->char('subj_group_id', 100);
            $table->timestamps();
            $table->softDeletes();


            $table->primary('group_subj_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_group_subjs');
    }
}

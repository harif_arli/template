<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_subjects', function (Blueprint $table) {
            $table->char('subj_id',10);
            $table->char('subj_name_th',100);
            $table->char('subj_name_en',100);
            $table->integer('cr_lec');
            $table->integer('cr_lab');
            $table->integer('credit');
            $table->string('detail');
            $table->char('group_subj_id',100);
            $table->timestamps();
            $table->softDeletes();


            $table->primary('subj_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_subjects');
    }
}

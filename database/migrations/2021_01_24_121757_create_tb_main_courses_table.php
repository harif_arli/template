<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbMainCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_main_courses', function (Blueprint $table) {
            $table->char('mc_id',2);
            $table->char('mc_name',255);
            $table->char('mc_year',50);
            $table->timestamps();
            $table->softDeletes();

            $table->primary('mc_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_main_courses');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_classrooms', function (Blueprint $table) {
            $table->char('clr_ID',7);
            $table->char('clr_Name',255);
            $table->char('clr_Type',50);
            $table->integer('clr_Seat');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('clr_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_classrooms');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbLecturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_lecturers', function (Blueprint $table) {
            $table->id();
            $table->char('t_ID',10);
            $table->char('subj_id',10);
            $table->integer('session');
            $table->integer('term');
            $table->char('year',20);
            $table->integer('seat');
            $table->timestamps();
            $table->softDeletes();

            //$table->primary(['t_ID','subj_id','session','term','year']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_lecturers');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_courses', function (Blueprint $table) {
            $table->id();
            $table->char('fac_id',2);
            $table->char('major_id',2);
            $table->char('mc_id',2);
            $table->char('mc_year',20);
            $table->integer('academic_y');
            $table->integer('term');
            $table->char('subj_id',10);
            $table->char('group_subj_id',10);
            $table->char('subj_group_id',2);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_courses');
    }
}

-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 23, 2021 at 06:39 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_provice`
--

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `province_id` bigint(20) UNSIGNED NOT NULL,
  `province_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`province_id`, `province_name`, `created_at`, `updated_at`) VALUES
(1, 'กรุงเทพมหานคร', NULL, NULL),
(2, 'สมุทรปราการ', NULL, NULL),
(3, 'นนทบุรี', NULL, NULL),
(4, 'ปทุมธานี', NULL, NULL),
(5, 'พระนครศรีอยุธยา', NULL, NULL),
(6, 'อ่างทอง', NULL, NULL),
(7, 'ลพบุรี', NULL, NULL),
(8, 'สิงห์บุรี', NULL, NULL),
(9, 'ชัยนาท', NULL, NULL),
(10, 'สระบุรี', NULL, NULL),
(11, 'ชลบุรี', NULL, NULL),
(12, 'ระยอง', NULL, NULL),
(13, 'จันทบุรี', NULL, NULL),
(14, 'ตราด', NULL, NULL),
(15, 'ฉะเชิงเทรา', NULL, NULL),
(16, 'ปราจีนบุรี', NULL, NULL),
(17, 'นครนายก', NULL, NULL),
(18, 'สระแก้ว', NULL, NULL),
(19, 'นครราชสีมา', NULL, NULL),
(20, 'บุรีรัมย์', NULL, NULL),
(21, 'สุรินทร์', NULL, NULL),
(22, 'ศรีสะเกษ', NULL, NULL),
(23, 'อุบลราชธานี', NULL, NULL),
(24, 'ยโสธร', NULL, NULL),
(25, 'ชัยภูมิ', NULL, NULL),
(26, 'อำนาจเจริญ', NULL, NULL),
(27, 'หนองบัวลำภู', NULL, NULL),
(28, 'ขอนแก่น', NULL, NULL),
(29, 'อุดรธานี', NULL, NULL),
(30, 'เลย', NULL, NULL),
(31, 'หนองคาย', NULL, NULL),
(32, 'มหาสารคาม', NULL, NULL),
(33, 'ร้อยเอ็ด', NULL, NULL),
(34, 'กาฬสินธุ์', NULL, NULL),
(35, 'สกลนคร', NULL, NULL),
(36, 'นครพนม', NULL, NULL),
(37, 'มุกดาหาร', NULL, NULL),
(38, 'เชียงใหม่', NULL, NULL),
(39, 'ลำพูน', NULL, NULL),
(40, 'ลำปาง', NULL, NULL),
(41, 'อุตรดิตถ์', NULL, NULL),
(42, 'แพร่', NULL, NULL),
(43, 'น่าน', NULL, NULL),
(44, 'พะเยา', NULL, NULL),
(45, 'เชียงราย', NULL, NULL),
(46, 'แม่ฮ่องสอน', NULL, NULL),
(47, 'นครสวรรค์', NULL, NULL),
(48, 'อุทัยธานี', NULL, NULL),
(49, 'กำแพงเพชร', NULL, NULL),
(50, 'ตาก', NULL, NULL),
(51, 'สุโขทัย', NULL, NULL),
(52, 'พิษณุโลก', NULL, NULL),
(53, 'พิจิตร', NULL, NULL),
(54, 'เพชรบูรณ์', NULL, NULL),
(55, 'ราชบุรี', NULL, NULL),
(56, 'กาญจนบุรี', NULL, NULL),
(57, 'สุพรรณบุรี', NULL, NULL),
(58, 'นครปฐม', NULL, NULL),
(59, 'สมุทรสาคร', NULL, NULL),
(60, 'สมุทรสงคราม', NULL, NULL),
(61, 'เพชรบุรี', NULL, NULL),
(62, 'ประจวบคีรีขันธ์', NULL, NULL),
(63, 'นครศรีธรรมราช', NULL, NULL),
(64, 'กระบี่', NULL, NULL),
(65, 'พังงา', NULL, NULL),
(66, 'ภูเก็ต', NULL, NULL),
(67, 'สุราษฎร์ธานี', NULL, NULL),
(68, 'ระนอง', NULL, NULL),
(69, 'ชุมพร', NULL, NULL),
(70, 'สงขลา', NULL, NULL),
(71, 'สตูล', NULL, NULL),
(72, 'ตรัง', NULL, NULL),
(73, 'พัทลุง', NULL, NULL),
(74, 'ปัตตานี', NULL, NULL),
(75, 'ยะลา', NULL, NULL),
(76, 'นราธิวาส', NULL, NULL),
(77, 'บึงกาฬ', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`province_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `province_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

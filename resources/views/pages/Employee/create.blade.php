{{-- Extends layout --}}
@extends('layout.default')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/css/uikit.css">
<link rel="stylesheet" href="{{ asset('jquery.Thailand.js/dist/jquery.Thailand.min.css') }}">
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-33058582-1', 'auto', {
        'name': 'Main'
    });
    ga('Main.send', 'event', 'jquery.Thailand.js', 'GitHub', 'Visit');
</script>


{{-- Content --}}
@section('content')
    <form class="form" id="kt_form" action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
        <div class="card card-custom card-sticky" id="kt_page_sticky_card">
            <!--begin::Form-->
            <div class="card-header">
                <div class="card-toolbar">
                    <ul class="nav nav-pills" role="tablist">
                        <li role="presentation" class="active" id="pills-home"><a href="#home" aria-controls="home"
                                                                                  role="tab" data-toggle="pill">ข้อมูลส่วนตัว</a>
                        </li>
                        <li role="presentation" id="pills-profile"><a href="#profile"
                                                                      aria-controls="profile"
                                                                      role="tab" data-toggle="tab">ที่อยู่</a>
                        </li>
                        <li role="presentation" id="pills-messages"><a href="#messages" aria-controls="messages"
                                                                       role="tab" data-toggle="tab">ประวัติการทำงาน</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                @csrf
                <div>
                    <div class="col-xl-10">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">

                                <div class="my-5">
                                    <div class="form-group row">
                                        <div class="col-6"></div>
                                        <div class="col-6">
                                            <div class="image-input image-input-outline" id="kt_image_2">
                                                <div class="image-input-wrapper"
                                                     style=""></div>

                                                <label
                                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                    data-action="change" data-toggle="tooltip" title=""
                                                    data-original-title="Change Image">
                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                    <input type="file" accept=".png, .jpg, .jpeg" name="empt_img"
                                                           id="empt_img"/>
                                                    <input type="hidden" name="empt_img" id="empt_img"/>
                                                    <span class="text-danger form-error"></span>
                                                </label>
                                                <span
                                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow form-error"
                                                    data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                                            </div>
                                            <label class="col-9">เพิ่มรูป</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-col-form-label">รหัสเจ้าหน้าที่</label>
                                        <div class="col-5">
                                            <input class="form-control input-int" type="text" name="empt_ID" value=""
                                                   id="empt_ID" maxlength="10">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class=" col-2 col-form-label">คำนำหน้า</label>
                                        <div class="col-2">
                                            <select class="form-control form-control" id="empt_title" name="empt_title">
                                                <option value="">คำนำหน้า</option>
                                                <option value="นาย">นาย</option>
                                                <option value="นาย">นาง</option>
                                                <option value="นาย">นางสาว</option>
                                            </select>
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ชื่อ</label>
                                        <div class="col-4">
                                            <input class="form-control " type="text" name="empt_Name_th" value=""
                                                   id="empt_Name_th">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">นามสกุล</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="empt_Lname_th" value=""
                                                   id="empt_Lname_th">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ชื่อภาษาอังกฤษ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="empt_Name_en" value=""
                                                   id="empt_Name_en">

                                            <span class="text-danger form-error"></span>

                                        </div>
                                        <label class="col-2 col-form-label">สกุลภาษาอังกฤษ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" value="" name="empt_Lname_en"
                                                   id="empt_Lname_en">

                                            <span class="text-danger form-error"></span>


                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">เลขบัตรประจำตัวประชาชน</label>
                                        <div class="col-4">

                                            <input class="form-control  input-int" type="text" name="empt_p_ID" value=""
                                                   id="empt_p_ID" maxlength="13">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">เบอร์โทร</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="empt_tel" value=""
                                                   id="empt_tel" maxlength="10">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">วันเกิด</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="birthday" value=""
                                                   id="birthday">

                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">อีเมล์</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="email" value="" id="email">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">

                                        <label class="col-2 col-form-label">ศาสนา</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="religion" id="religion">
                                                <option value="">เลือกศาสนา</option>
                                                <option value="อิสลาม">อิสลาม</option>
                                                <option value="B">พุทธ</option>
                                                <option value="O">คริสต์</option>
                                                <option value="AB">พราหมณ์-ฮินดู</option>
                                            </select>
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">กรุ๊ปเลือด</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="blood_type" id="blood_type">
                                                <option value="">เลือกกรุ๊ปเลือด</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="O">O</option>
                                                <option value="AB">AB</option>
                                            </select>
                                            <span class="text-danger form-error"></span>
                                        </div>

                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ความพิการ</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="disability" id="disability">
                                                <option value="">เลือกความพิการ</option>
                                                <option value="ไม่มี">ไม่มี</option>
                                                <option value="ทางการเห็น">ทางการเห็น</option>
                                                <option value="การได้ยิน">การได้ยิน</option>
                                                <option value="ทางร่างกาย">ทางร่างกาย</option>
                                                <option value="ทางการเรียนรู้">ทางการเรียนรู้</option>
                                                <option value="ทางการพูด และภาษา">ทางการพูด และภาษา</option>
                                                <option value="ทางพฤติกรรม หรืออารมณ์">ทางพฤติกรรม หรืออารมณ์</option>
                                                <option value="ออทิสติก">ออทิสติก</option>
                                            </select>
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">

                                        <a id="btnNextToAddress" aria-controls="profile" role="tablist"
                                           class="btn btn-primary font-weight-bolder next-tab" data-toggle="tab"> ถัดไป
                                            <i class="ki ki-long-arrow-next"></i></a>


                                    </div>
                                </div>
                                <br><br>

                            </div>

                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ที่อยู่ตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="addr_birthplace" value=""
                                                   id="addr_birthplace">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ตำบลตามตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_t" class="form-control" type="text" id="district">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">อำเภอตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_city" class="form-control" type="text" id="amphoe">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">จังหวัดตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_prov" class="form-control" type="text" id="province">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">รหัสไปรษณีย์ตาม ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_post" class="form-control" type="text" id="zipcode">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="showChecked"
                                           name="showHideTextbox" value="show">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="form-check-label" for="showChecked">ที่อยู่ปัจจุบันเหมือนกันกับที่อยู่ตามบัตรประชาชน</label>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ที่อยู่ปัจจุบัน</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="addr_cerrent" value=""
                                                   id="addr_cerrent">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ตำบล</label>
                                        <div class="col-4">
                                            <input name="addr_c_t" class="form-control" type="text" id="district1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">อำเภอ</label>
                                        <div class="col-4">
                                            <input name="addr_c_city" class="form-control" type="text" id="amphoe1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">จังหวัด</label>
                                        <div class="col-4">
                                            <input name="addr_c_prov" class="form-control" type="text" id="province1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">รหัสไปรษณีย์</label>
                                        <div class="col-4">
                                            <input name="addr_c_post" class="form-control" type="text" id="zipcode1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">

                                        <a id="backhome" aria-controls="home" role="tab"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <a id="btnNextTo" aria-controls="messages" role="tab"
                                           class="btn btn-primary font-weight-bolder next-tab" data-toggle="tab"> ถัดไป
                                            <i class="ki ki-long-arrow-next"></i></a>

                                    </div>
                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">วันที่เรีมทำงาน</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="work_date" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">วันที่บรรจุ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="date_permit" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">

                                        <label class="col-2 col-form-label">วันที่เกษียณอายุ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="retire_date" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">ความเชียวชาญ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="expertise" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a id="backaddrees" aria-controls="home" role="tab"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
                                            <i class="ki ki-check icon-sm"></i>
                                            บันทึก
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="separator separator-dashed my-10"></div>

                        </div>

                        <div class="col-xl-2"></div>
                        <!--end::Form-->
    </form>
    </div>


    </div>

    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
{{--    <script>--}}
{{--        $(document).ready(function () {--}}
{{--            $('#save_form').on('click', function () {--}}
{{--                // window.location = "/purchase-order";--}}
{{--                // $('#kt_form').submit();--}}
{{--            });--}}

{{--            $('.input-int').mask("#0", {reverse: true});--}}

{{--            $('#kt_form input, #kt_form select,#kt_form checkbox ').on('keyup change checked', function () {--}}
{{--                // console.log("check key", this.value)--}}
{{--                if (this.id == "empt_img") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_img').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_img~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_ID") {--}}
{{--                    if (this.value.length == 10) {--}}
{{--                        $('#empt_ID').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_ID~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_title") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_title').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_title~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_Name_th") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_Name_th').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_Name_th~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_Lname_th") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_Lname_th').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_Lname_th~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_Name_en") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_Name_en').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_Name_en~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_Lname_en") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_Lname_en').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_Lname_en~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_p_ID") {--}}
{{--                    if (this.value.length == 13) {--}}
{{--                        $('#empt_p_ID').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_p_ID~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "empt_tel") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#empt_tel').css("borderColor", "#61db2e");--}}
{{--                        $('#empt_tel~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "religion") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#religion').css("borderColor", "#61db2e");--}}
{{--                        $('#religion~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "blood_type") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#blood_type').css("borderColor", "#61db2e");--}}
{{--                        $('#blood_type~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "disability") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#disability').css("borderColor", "#61db2e");--}}
{{--                        $('#disability~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "birthday") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#birthday').css("borderColor", "#61db2e");--}}
{{--                        $('#birthday~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "email") {--}}
{{--                    if (!isEmail(this.value)) {--}}
{{--                        $('#email').css("borderColor", "red")--}}
{{--                        $('#email ~ span.form-error').text('อีเมลไม่ถูกต้อง');--}}
{{--                        check = false;--}}
{{--                    } else if (isEmail(this.value)) {--}}
{{--                        $('#email').css("borderColor", "#61db2e")--}}
{{--                        $('#email ~ span.form-error').text('');--}}
{{--                        check = false;--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "addr_birthplace") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#addr_birthplace').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_birthplace~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "province") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#province').css("borderColor", "#61db2e");--}}
{{--                        $('#province~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "district") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#district').css("borderColor", "#61db2e");--}}
{{--                        $('#district~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "subdistrict") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#subdistrict').css("borderColor", "#61db2e");--}}
{{--                        $('#subdistrict~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "addr_b_post") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#addr_b_post').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_b_post~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "showChecked") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#addr_cerrent').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_cerrent~span.form-error').text('');--}}
{{--                        $('#province1').css("borderColor", "#61db2e");--}}
{{--                        $('#province1~span.form-error').text('');--}}
{{--                        $('#district1').css("borderColor", "#61db2e");--}}
{{--                        $('#district1~span.form-error').text('');--}}
{{--                        $('#subdistrict1').css("borderColor", "#61db2e");--}}
{{--                        $('#subdistrict1~span.form-error').text('');--}}
{{--                        $('#addr_c_post').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_c_post~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "addr_cerrent") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#addr_cerrent').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_cerrent~span.form-error').text('');--}}

{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "province1") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#province1').css("borderColor", "#61db2e");--}}
{{--                        $('#province1~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "district1") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#district1').css("borderColor", "#61db2e");--}}
{{--                        $('#district1~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "district1") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#district1').css("borderColor", "#61db2e");--}}
{{--                        $('#district1~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "subdistrict1") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#subdistrict1').css("borderColor", "#61db2e");--}}
{{--                        $('#subdistrict1~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--                if (this.id == "addr_c_post") {--}}
{{--                    if (this.value) {--}}
{{--                        $('#addr_c_post').css("borderColor", "#61db2e");--}}
{{--                        $('#addr_c_post~span.form-error').text('');--}}
{{--                    }--}}
{{--                }--}}
{{--            })--}}


{{--            $('#btnNextToAddress').on("click", function () {--}}

{{--                $('input, select').css("borderColor", "#ccc");--}}
{{--                $('input~span.form-error, select~span.form-error').text('');--}}
{{--                let empt_img = $('#empt_img').val();--}}
{{--                let empt_ID = $('#empt_ID').val();--}}
{{--                let empt_title = $('#empt_title').val();--}}
{{--                let empt_Name_th = $('#empt_Name_th').val();--}}
{{--                let empt_Lname_th = $('#empt_Lname_th').val();--}}
{{--                let empt_Name_en = $('#empt_Name_en').val();--}}
{{--                let empt_Lname_en = $('#empt_Lname_en').val();--}}
{{--                let empt_p_ID = $('#empt_p_ID').val();--}}
{{--                let empt_tel = $('#empt_tel').val();--}}
{{--                let birthday = $('#birthday').val();--}}
{{--                let email = $('#email').val();--}}
{{--                let religion = $('#religion').val();--}}
{{--                let blood_type = $('#blood_type').val();--}}
{{--                let disability = $('#disability').val();--}}
{{--                let check = true;--}}
{{--                if (!empt_img) {--}}
{{--                    $('#empt_img').css("borderColor", "red").focus();--}}
{{--                    $('#empt_img ~ span.form-error').text('เพิ่มรูป');--}}
{{--                    check = false;--}}

{{--                }--}}
{{--                if (!empt_ID) {--}}
{{--                    $('#empt_ID').css("borderColor", "red").focus();--}}
{{--                    $('#empt_ID ~ span.form-error').text('กรอกรหัสพนักงาน');--}}
{{--                    check = false;--}}
{{--                } else if (empt_ID.length < 10) {--}}
{{--                    $('#empt_ID').css("borderColor", "red").focus();--}}
{{--                    $('#empt_ID ~ span.form-error').text('รหัสไม่ถูกต้อง');--}}
{{--                }--}}
{{--                if (!empt_title) {--}}
{{--                    $('#empt_title').css("borderColor", "red")--}}
{{--                    $('#empt_title ~ span.form-error').text('เลือกคำนำหน้า');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!empt_Name_th) {--}}
{{--                    $('#empt_Name_th').css("borderColor", "red")--}}
{{--                    $('#empt_Name_th ~ span.form-error').text('กรอกชื่อ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!empt_Lname_th) {--}}
{{--                    $('#empt_Lname_th').css("borderColor", "red")--}}
{{--                    $('#empt_Lname_th ~ span.form-error').text('กรอกนามสกุล');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!empt_Name_en) {--}}
{{--                    $('#empt_Name_en').css("borderColor", "red")--}}
{{--                    $('#empt_Name_en ~ span.form-error').text('กรอกชื่อภาษาอังกฤษ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!empt_Lname_en) {--}}
{{--                    $('#empt_Lname_en').css("borderColor", "red")--}}
{{--                    $('#empt_Lname_en ~ span.form-error').text('กรอกนามสกุลภาษาอังกฤษ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!empt_p_ID) {--}}
{{--                    $('#empt_p_ID').css("borderColor", "red")--}}
{{--                    $('#empt_p_ID ~ span.form-error').text('กรอกเลขบัตรประชาชน');--}}
{{--                    check = false;--}}
{{--                } else if (empt_p_ID.length < 13) {--}}
{{--                    $('#empt_p_ID').css("borderColor", "red").focus();--}}
{{--                    $('#empt_p_ID ~ span.form-error').text('เลขบัตรประชาชนไม่ถูกต้อง');--}}
{{--                }--}}
{{--                if (!empt_tel) {--}}
{{--                    $('#empt_tel').css("borderColor", "red")--}}
{{--                    $('#empt_tel ~ span.form-error').text('กรอกเบอร์โทรศัพท์');--}}
{{--                    check = false;--}}
{{--                } else if (empt_tel.length < 10) {--}}
{{--                    $('#empt_tel').css("borderColor", "red").focus();--}}
{{--                    $('#empt_tel ~ span.form-error').text('เบอร์โทรศัพท์ไม่ถูกต้อง');--}}
{{--                }--}}
{{--                if (!birthday) {--}}
{{--                    $('#birthday').css("borderColor", "red")--}}
{{--                    $('#birthday ~ span.form-error').text('กรอกวันเกิด');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!email) {--}}
{{--                    $('#email').css("borderColor", "red")--}}
{{--                    $('#email ~ span.form-error').text('กรอกอีเมล์');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!religion) {--}}
{{--                    $('#religion').css("borderColor", "red")--}}
{{--                    $('#religion ~ span.form-error').text('เลือกศาสนา');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!blood_type) {--}}
{{--                    $('#blood_type').css("borderColor", "red")--}}
{{--                    $('#blood_type ~ span.form-error').text('เลือกกรุปเลือด');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!disability) {--}}
{{--                    $('#disability').css("borderColor", "red")--}}
{{--                    $('#disability ~ span.form-error').text('เลือกความพิการ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (check) {--}}
{{--                    $('#pills-profile a').trigger('click')--}}
{{--                }--}}
{{--            })--}}


{{--            $('#btnNextTo').on("click", function () {--}}

{{--                $('input, select').css("borderColor", "#ccc");--}}
{{--                $('input~span.form-error, select~span.form-error').text('');--}}

{{--                let addr_birthplace = $('#addr_birthplace').val();--}}
{{--                let province = $('#province').val();--}}
{{--                let district = $('#district').val();--}}
{{--                let subdistrict = $('#subdistrict').val();--}}
{{--                let addr_b_post = $('#addr_b_post').val();--}}
{{--                let addr_cerrent = $('#addr_cerrent').val();--}}
{{--                let province1 = $('#province1').val();--}}
{{--                let district1 = $('#district1').val();--}}
{{--                let subdistrict1 = $('#subdistrict1').val();--}}
{{--                let addr_c_post = $('#addr_c_post').val();--}}
{{--                let check = true;--}}
{{--                if (!addr_birthplace) {--}}
{{--                    $('#addr_birthplace').css("borderColor", "red").focus();--}}
{{--                    $('#addr_birthplace ~ span.form-error').text('กรอกที่อยู่');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!province) {--}}
{{--                    $('#province').css("borderColor", "red")--}}
{{--                    $('#province ~ span.form-error').text('เลือกจังหวัด');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!district) {--}}
{{--                    $('#district').css("borderColor", "red")--}}
{{--                    $('#district ~ span.form-error').text('เลือกอำเภอ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!subdistrict) {--}}
{{--                    $('#subdistrict').css("borderColor", "red")--}}
{{--                    $('#subdistrict ~ span.form-error').text('เลือกตำบล');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!addr_b_post) {--}}
{{--                    $('#addr_b_post').css("borderColor", "red")--}}
{{--                    $('#addr_b_post ~ span.form-error').text('กรอกรหัสใปรษรีย์');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!addr_cerrent) {--}}
{{--                    $('#addr_cerrent').css("borderColor", "red")--}}
{{--                    $('#addr_cerrent ~ span.form-error').text('กรอกที่อยู่');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!province1) {--}}
{{--                    $('#province1').css("borderColor", "red")--}}
{{--                    $('#province1 ~ span.form-error').text('เลือกจังหวัด');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!district1) {--}}
{{--                    $('#district1').css("borderColor", "red")--}}
{{--                    $('#district1 ~ span.form-error').text('เลือกจังหวัด');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!province1) {--}}
{{--                    $('#province1').css("borderColor", "red")--}}
{{--                    $('#province1 ~ span.form-error').text('เลือกอำเภอ');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!subdistrict1) {--}}
{{--                    $('#subdistrict1').css("borderColor", "red")--}}
{{--                    $('#subdistrict1 ~ span.form-error').text('เลือกอำเภอปัจจุบัน');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!subdistrict1) {--}}
{{--                    $('#subdistrict1').css("borderColor", "red")--}}
{{--                    $('#subdistrict1 ~ span.form-error').text('เลือกตำบลปัจจุบัน');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (!addr_c_post) {--}}
{{--                    $('#addr_c_post').css("borderColor", "red")--}}
{{--                    $('#addr_c_post ~ span.form-error').text('กรอกรหัสใปรษรีย์ปัจจุบัน');--}}
{{--                    check = false;--}}
{{--                }--}}
{{--                if (check) {--}}
{{--                    $('#pills-messages a').trigger('click')--}}
{{--                }--}}
{{--            })--}}
{{--        });--}}

{{--        $('#backhome').click(function () {--}}
{{--            $('#pills-home a').trigger('click')--}}
{{--        });--}}

{{--        $('#backaddrees').click(function () {--}}
{{--            $('#pills-profile a').trigger('click')--}}
{{--        });--}}
{{--        --}}

{{--    </script>--}}
    <script>
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        var avatar2 = new KTImageInput('kt_image_2');
    </script>
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/></link>
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>


        $(".back-tab").click(function () {
            var tab_name = $(this).attr('href').replace("#", "");
            $(".nav-pills li").removeClass('active');
            $("#pills-" + tab_name).addClass('active');
        });


        setInterval(function () {
            check_part1()
        }, 300);
        setInterval(function () {
            check_part2()
        }, 300);

        });
    </script>

    <script>
        $(document).ready(function () {
            $("#showChecked").click(function () {

                if ($(this).is(":checked")) {

                    $("#addr_cerrent").val($("#addr_birthplace").val());
                    $("#district1").val($("#district").val());
                    $("#amphoe1").val($("#amphoe").val());
                    $("#province1").val($("#province").val());
                    $("#zipcode1").val($("#zipcode").val());
                } else {
                }
            });
        });
    </script>

    <!-- dependencies for zip mode -->
    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/zip.js/zip.js') }}"></script>
    <!-- / dependencies for zip mode -->

    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/JQL.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/typeahead.bundle.js') }}"></script>

    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dist/jquery.Thailand.min.js') }}"></script>

    <script type="text/javascript">
        /******************\
         *     DEMO 1     *
         \******************/
        // demo 1: load database from json. if your server is support gzip. we recommended to use this rather than zip.
        // for more info check README.md

        $.Thailand({
            database: '{{ asset('jquery.Thailand.js/database/db.json') }}',


            $district: $('#kt_form [name="addr_b_t"]'),
            $amphoe: $('#kt_form [name="addr_b_city"]'),
            $province: $('#kt_form [name="addr_b_prov"]'),
            $zipcode: $('#kt_form [name="addr_b_post"]'),

            onDataFill: function (data) {
                console.info('Data Filled', data);
            },

            onLoad: function () {
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#kt_form [name="addr_b_t"]').change(function () {
            console.log('ตำบล', this.value);
        });
        $('#kt_form [name="addr_b_city"]').change(function () {
            console.log('อำเภอ', this.value);
        });
        $('#kt_form [name="addr_b_prov"]').change(function () {
            console.log('จังหวัด', this.value);
        });
        $('#kt_form [name="addr_b_post"]').change(function () {
            console.log('รหัสไปรษณีย์', this.value);
        });

        $.Thailand({
            database: '{{ asset('jquery.Thailand.js/database/db.json') }}',


            $district: $('#kt_form [name="addr_c_t"]'),
            $amphoe: $('#kt_form [name="addr_c_city"]'),
            $province: $('#kt_form [name="addr_c_prov"]'),
            $zipcode: $('#kt_form [name="addr_c_post"]'),

            onDataFill: function (data) {
                console.info('Data Filled', data);
            },

            onLoad: function () {
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });
    </script>




@endsection

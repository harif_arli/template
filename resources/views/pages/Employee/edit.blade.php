{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')

    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <!--begin::Form-->
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                    แก้ใขข้อมูลเจ้าหน้าที่<i class="mr-2"></i>
                </h3>
            </div>
            <div class="card-toolbar">
                <a href="{{route('teacher.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
                    <i class="ki ki-long-arrow-back icon-sm"></i>
                    ยกเลิก
                </a>
                <div class="btn-group">
                    <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
                        <i class="ki ki-check icon-sm"></i>
                        แก้ใขข้อมูล
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form class="form" id="kt_form" action="{{action('EmployeeController@update',$id)}}" method="POST"
                  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xl-10">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                        <div class="form-group row">
                            <label class="col-3"></label>
                            <div class="col-9">
                                <div class="image-input image-input-outline" id="kt_image_2">
                                    <div class="image-input-wrapper"
                                         style="background-image: url({{getImgPathFirebase($employee->empt_img)}})">
                                        @if(!empty($employee->empt_img))
                                            <img src="{{ Storage::url($employee->empt_img) }}" alt="" width="100"
                                                 height="100">
                                        @endif
                                    </div>

                                    <label
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="change" data-toggle="tooltip" title=""
                                        data-original-title="Change Image">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" accept=".png, .jpg, .jpeg" name="empt_img"/>
                                        <input type="hidden" name="empt_img_remove"/>

                                    </label>

                                    <span
                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                        data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-col-form-label">รหัสเจ้าหน้าที่</label>
                                <div class="col-5">
                                    <input class="form-control" type="text" name="empt_ID"
                                           value="{{$employee->empt_ID}}" id="example-text-input">
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class=" col-2 col-form-label">คำนำหน้า</label>
                                <div class="col-2">
                                    <select class="form-control form-control" name="empt_title">
                                        <option value="{{$employee->empt_title}}">{{$employee->empt_title}}</option>
                                        <option value="นาย">นาย</option>
                                        <option value="นาย">นาง</option>
                                        <option value="นาย">นางสาว</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">ชื่อ</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_Name_th"
                                           value="{{$employee->empt_Name_th}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">นามสกุล</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_Lname_th"
                                           value="{{$employee->empt_Lname_th}}" id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">ชื่อภาษาอังกฤษ</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_Name_en"
                                           value="{{$employee->empt_Name_en}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">สกุลภาษาอังกฤษ</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_Lname_en"
                                           value="{{$employee->empt_Lname_en}}" id="example-text-input">
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">เลขบัตรประจำตัวประชาชน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_p_ID"
                                           value="{{$employee->empt_p_ID}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">เบอร์โทร</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="empt_tel"
                                           value="{{$employee->empt_tel}}" id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">วันเกิด</label>
                                <div class="col-4">
                                    <input class="form-control" type="date" name="birthday"
                                           value="{{$employee->birthday}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">อีเมล์</label>
                                <div class="col-4">
                                    <input class="form-control" type="email" name="email" value="{{$employee->email}}"
                                           id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">

                                <label class="col-2 col-form-label">ศาสนา</label>
                                <div class="col-4">
                                    <select class="form-control form-control" name="religion">
                                        <option value="{{$employee->religion}}">{{$employee->religion}}</option>
                                        <option value="อิสลาม">อิสลาม</option>
                                        <option value="B">พุทธ</option>
                                        <option value="O">คริสต์</option>
                                        <option value="AB">พราหมณ์-ฮินดู</option>
                                    </select>
                                </div>
                                <label class="col-2 col-form-label">กรุ๊ปเลือด</label>
                                <div class="col-4">
                                    <select class="form-control form-control" name="blood_type">
                                        <option value="{{$employee->blood_type}}">{{$employee->blood_type}}</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="O">O</option>
                                        <option value="AB">AB</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">ความพิการ</label>
                                <div class="col-4">
                                    <select class="form-control form-control" name="disability">
                                        <option value="{{$employee->disability}}">{{$employee->disability}}</option>
                                        <option value="ไม่มี">ไม่มี</option>
                                        </option>
                                        <option value="ทางการเห็น">ทางการเห็น</option>
                                        <option value="การได้ยิน">การได้ยิน</option>
                                        <option value="ทางร่างกาย">ทางร่างกาย</option>
                                        <option value="ทางการเรียนรู้">ทางการเรียนรู้</option>
                                        <option value="ทางการพูด และภาษา">ทางการพูด และภาษา</option>
                                        <option value="ทางพฤติกรรม หรืออารมณ์">ทางพฤติกรรม หรืออารมณ์</option>
                                        <option value="ออทิสติก">ออทิสติก</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">ที่อยู่ตามบัตร ปปช</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_birthplace"
                                           value="{{$employee->addr_birthplace}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">ตำบลตามบัตรประจำตัวประชาชน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_b_t"
                                           value="{{$employee->district}}" id="example-text-input">
                                </div>

                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">อำเภอตามบัตร ปปช</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_b_city"
                                           value="{{$employee->amphoe}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">จังหวัดตามบัตรประจำตัวประชาชน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_b_prov"
                                           value="{{$employee->province}}" id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">รหัสไปรษณีย์ ปปช</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_b_post"
                                           value="{{$employee->zipcode}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">ที่อยู่ปัจจุบัน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_cerrent"
                                           value="{{$employee->addr_cerrent}}" id="example-text-input">
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">ตำบลปัจจุบัน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text " name="addr_c_t"
                                           value="{{$employee->district1}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">อำเภอปัจจุบัน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_c_city"
                                           value="{{$employee->amphoe1}}" id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">จังหวัดปัจจุบัน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_c_prov"
                                           value="{{$employee->province1}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">รหัสไปรษณีย์ปัจจุบัน</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="addr_c_post"
                                           value="{{$employee->zipcode1}}" id="example-text-input">
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">
                                <label class="col-2 col-form-label">วันที่เรีมทำงาน</label>
                                <div class="col-4">
                                    <input class="form-control" type="date" name="work_date"
                                           value="{{$employee->work_date}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">วันที่บรรจุ</label>
                                <div class="col-4">
                                    <input class="form-control" type="date" name="date_permit"
                                           value="{{$employee->date_permit}}" id="example-text-input">
                                </div>
                            </div>
                        </div>

                        <div class="my-5">
                            <div class="form-group row">

                                <label class="col-2 col-form-label">วันที่เกษียณอายุ</label>
                                <div class="col-4">
                                    <input class="form-control" type="date" name="retire_date"
                                           value="{{$employee->retire_date}}" id="example-text-input">
                                </div>
                                <label class="col-2 col-form-label">ความเชียวชาญ</label>
                                <div class="col-4">
                                    <input class="form-control" type="text" name="expertise"
                                           value="{{$employee->expertise}}" id="example-text-input">
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="_method" value="PATCH"/>

                        <div class="separator separator-dashed my-10"></div>

                    </div>
                    <div class="col-xl-2"></div>
                    <!--end::Form-->
            </form>
        </div>


    </div>

    </div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#save_form').on('click', function () {
                // window.location = "/purchase-order";
                $('#kt_form').submit();
            });

            FormValidation.formValidation(
                document.getElementById('kt_form'), {
                    fields: {
                        empt_img: {
                            validators: {
                                notEmpty: {
                                    message: 'กรุณาเพิ่มรูป'
                                }
                            }
                        },
                        empt_ID: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกรหัสพนักงาน'
                                },
                                integer: {
                                    message: 'เฉพาะตัวเลข',
                                },
                                integer: {
                                    message: 'เฉพาะตัวเลข'
                                }
                            }
                        },

                        empt_title: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกคำนำหน้า'
                                }
                            }
                        },


                        empt_Name_th: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกชื่อ'
                                }
                            }
                        },

                        empt_Lname_th: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกนามสกุล'
                                }
                            }
                        },


                        empt_Name_en: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกชื่อภาษาอังกฤษ'
                                }
                            }
                        },

                        empt_Lname_en: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกนามสกุลภาษาอังกฤษ'
                                }
                            }
                        },


                        empt_p_ID: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกเลขประจำตัวประชาชน'
                                },
                                integer: {
                                    message: 'เฉพาะตัวเลข'
                                }
                            }
                        },

                        empt_tel: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกเบอร์โทรศัพท์'
                                },
                                integer: {
                                    message: 'เฉพาะตัวเลข'
                                }
                            }
                        },

                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกอีเมล์'
                                },
                                emailAddress: {
                                    message: 'อีเมล์ไม่ถูกต้อง'
                                }
                            }
                        },

                        religion: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกศาสนา'
                                }
                            }
                        },

                        blood_type: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกกรุ๊ปเลือก'
                                }
                            }
                        },


                        disability: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกความพิการ'
                                }
                            }
                        },

                        addr_birthplace: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกที่อยู่'
                                }
                            }
                        },

                        addr_b_prov: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกจังหวัดตามบัตร ปปปช'
                                }
                            }
                        },

                        addr_b_city: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกอำเภอตามบัตร ปปช'
                                }
                            }
                        },


                        addr_b_t: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกตำบลตามบัตร ปปช'
                                }
                            }
                        },


                        addr_b_post: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกรหัสไปรษณีย์ตาม ปปช'
                                }
                            }
                        },


                        addr_cerrent: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกที่อยู่ปัจจุบัน'
                                }
                            }
                        },

                        addr_c_prov: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกจังหวัดอยู่ปัจจุบัน'
                                }
                            }
                        },

                        addr_c_city: {
                            validators: {
                                notEmpty: {
                                    message: 'เลือกอำเภอปัจจุบัน'
                                }
                            }
                        },


                        addr_c_t: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกตำบลปัจจุบัน'
                                }
                            }
                        },

                        addr_c_post: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกรหัสไปรษณีย์ปัจจุบัน'
                                }
                            }
                        },


                        birthday: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกวันเกิด'
                                }
                            }
                        },

                        work_date: {
                            validators: {
                                notEmpty: {
                                    message: ' กรอกวันเริ่มทำงาน '
                                }
                            }
                        },

                        date_permit: {
                            validators: {
                                notEmpty: {
                                    message: 'กรอกวันบรรจุ'
                                }
                            }
                        },

                        retire_date: {
                            validators: {
                                notEmpty: {
                                    message: ' กรอกวันที่เกษียณอายุ '
                                }
                            }
                        },

                        expertise: {
                            validators: {
                                notEmpty: {
                                    message: 'ความเชียวชาญ'
                                }
                            }
                        },

                    },

                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        // Bootstrap Framework Integration
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        // Validate fields when clicking the Submit button
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        // Submit the form when all fields are valid
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                    }
                }
            );
        });
        var avatar2 = new KTImageInput('kt_image_2');
    </script>


    <script>
        $('#province').change(function () {
            var province_id = this.value;
            if (province_id != '') {
                $.ajax({
                    url: "{{ route('get_district') }}",
                    method: "post",
                    data: {
                        province_id: province_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (data) {
                        if (data == '') {
                            alert('ไม่พบข้อมูลอำเภอ !!.');
                        } else {
                            var layout = '<option value="" selected>- เลือกเขต -</option>';
                            $.each(data, function (key, value) {
                                layout += '<option value=' + value.district_id + 'x' + value.postcode + '>' + value.district_name + '</option>';
                            });
                            $('#district').html(layout);
                            $('#postcode').val('');
                            $('#subdistrict').html('');
                        }
                    }
                })
            }
        });
        $('#district').change(function () {
            var str = this.value.split("x");
            var district_id = str[0];
            var postcode = str[1];

            if (district_id != '') {
                $.ajax({
                    url: "{{ route('get_subdistrict') }}",
                    method: "post",
                    data: {
                        district_id: district_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (res) {
                        if (res == '') {
                            alert('ไม่พบข้อมูลตำบล !!.');
                        } else {
                            var layout = '<option value="" selected>- เลือกตำบล -</option>';
                            $.each(res, function (key, value) {
                                layout += '<option value=' + value.sub_district_id + '>' + value.sub_district_name + '</option>';
                            });
                            $('#subdistrict').html(layout);
                            $('#postcode').val(postcode);
                        }
                    }
                })
            }
        });

    </script>
    <script>
        $('#province1').change(function () {
            var province_id = this.value;
            if (province_id != '') {
                $.ajax({
                    url: "{{ route('get_district') }}",
                    method: "post",
                    data: {
                        province_id: province_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (data) {
                        if (data == '') {
                            alert('ไม่พบข้อมูลอำเภอ !!.');
                        } else {
                            var layout = '<option value="" selected>- เลือกเขต -</option>';
                            $.each(data, function (key, value) {
                                layout += '<option value=' + value.district_id + 'x' + value.postcode + '>' + value.district_name + '</option>';
                            });
                            $('#district1').html(layout);
                            $('#postcode').val('');
                            $('#subdistrict').html('');
                        }
                    }
                })
            }
        });

        $('#district1').change(function () {
            var str = this.value.split("x");
            var district_id = str[0];
            var postcode = str[1];

            if (district_id != '') {
                $.ajax({
                    url: "{{ route('get_subdistrict') }}",
                    method: "post",
                    data: {
                        district_id: district_id,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function (res) {
                        if (res == '') {
                            alert('ไม่พบข้อมูลตำบล !!.');
                        } else {
                            var layout = '<option value="" selected>- เลือกตำบล -</option>';
                            $.each(res, function (key, value) {
                                layout += '<option value=' + value.sub_district_id + '>' + value.sub_district_name + '</option>';
                            });
                            $('#subdistrict1').html(layout);
                            $('#postcode').val(postcode);
                        }
                    }
                })
            }
        });

    </script>

    <script>
        $(document).ready(function () {
            $("#showChecked").click(function () {

                if ($(this).is(":checked")) {

                    $("#addr_c_post").val($("#addr_b_post").val());

                    $("#addr_cerrent").val($("#addr_birthplace").val());

                    document.getElementById("province1").selectedIndex = "2";
                    var province = $("#province").val();
                    var options = document.getElementById("province1").options;
                    for (var i = 0; i < options.length; i++) {
                        if (options[i].value == province) {
                            options[i].selected = true;
                            break;
                        }
                    }

                    var full_district = $("#district").html();
                    $("#district1").html(full_district);
                    var district = $("#district").val();
                    var options = document.getElementById("district1").options;
                    for (var i = 0; i < options.length; i++) {
                        if (options[i].value == district) {
                            options[i].selected = true;
                            break;
                        }
                    }

                    var full_subdistrict = $("#subdistrict").html();
                    $("#subdistrict1").html(full_subdistrict);
                    var subdistrict = $("#subdistrict").val();
                    var options = document.getElementById("subdistrict1").options;
                    for (var i = 0; i < options.length; i++) {
                        if (options[i].value == subdistrict) {
                            options[i].selected = true;
                            break;
                        }
                    }
                } else {
                }
            });
        });
    </script>



@endsection

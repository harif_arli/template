{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{action('GroupsubjectController@update',$id)}}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        เพิ่มสาขาวิชา<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('major.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">กลุ่มวิชา :</h3>
            <div class="form-group row">
                <label class="col-3">รหัสกลุ่มวิชา</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="group_subj_id" type="text" value="{{$groupsubject->group_subj_id}}" />
                </div>
              </div>
            <div class="form-group row">
              <label class="col-3">ชื่อกลุ่มวิชา</label>
              <div class="col-9">
                <input class="form-control form-control-solid" name="group_subj_name" type="text" value="{{$groupsubject->group_subj_name}}" />
              </div>
            </div>
          </div>

            <div class="form-group row">
                <label class="col-3">หมวดวิชา </label>
                <div class="col-9">
                  <select class="form-control form-control" name="subj_group_id">
                    <option value="{{$groupsubject->subj_group_id}}">{{$groupsubject->subjectgroup->subj_group_name}}</option>
                    @foreach($subjectgroup as $row )
                        <option value="{{$row->subj_group_id}}"> {{$row->subj_group_name }}</option>
                    @endforeach
                </select>
                </div>
              </div>
            </div>

        </div>
          <input type="hidden" name="_method" value="PATCH"/>
          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            group_subj_id: {
            validators: {
              notEmpty: {
                message: 'กรอกรหัสสาขา'
              },
               integer: {
                         message: 'The value is not an integer',
              }
            }
          },

          group_subj_name: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อสาขา'
              }
            }
          },

          subj_group_id: {
            validators: {
              notEmpty: {
                message: 'กรุณาเลือกหมวดวิชา'
              }
            }
          },


        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection

{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{action('SubjectController@update',$id)}}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        แก้ใขรายวิชา<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('subject.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">รายวิชา :</h3>
            <div class="form-group row">
                <label class="col-3">รหัสวิชา</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="subj_id" type="text" value="{{$subject->subj_id}}" />
                </div>
              </div>
            <div class="form-group row">
              <label class="col-3">ชื่อวิชาภาษาไทย</label>
              <div class="col-9">
                <input class="form-control form-control-solid" name="subj_name_th" type="text" value="{{$subject->subj_name_th}}" />
              </div>
            </div>

            <div class="form-group row">
                <label class="col-3">ชื่อวิชาภาษาอังกฤษ</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="subj_name_en" type="text" value="{{$subject->subj_name_en}}" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">หน่วยกิตบรรยาย</label>
                <div class="col-9">
                    <input class="form-control form-control-solid" id="credit" name="cr_lec" type="number" value="{{$subject->cr_lec}}" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">หน่วยกิตปฏิบัติการ</label>
                <div class="col-9">
                    <input class="form-control form-control-solid" id="credit" name="cr_lab" type="number" value="{{$subject->cr_lab}}" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">หน่วยกิต</label>
                <div class="col-9">
                    <input class="form-control form-control-solid" id="credit" name="credit" type="number" value="{{$subject->credit}}" />
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">คำอธิบายรายวิชา</label>
                <div class="col-9">
                  <textarea rows="5" name="detail"  id="detail" class="form-control form-control-solid">{{$subject->detail}}</textarea>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-3">กลุ่มวิชา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="group_subj_id">
                        <option value="{{$subject->group_subj_id}}">{{$subject->groupsubject->group_subj_name}}</option>
                        @foreach($groupsubject as $row )
			                <option value="{{$row->id}}">{{$row->group_subj_name }}</option>
		                @endforeach
                    </select>
                </div>
              </div>

          <input type="hidden" name="_method" value="PATCH"/>
          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            subj_id: {
            validators: {
              notEmpty: {
                message: 'กรอกรหัสสาขา'
              },
            }
          },

          subj_name_th: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อสาขา'
              }
            }
          },

          subj_name_en: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อสาขาภาษาอังกฤษ'
              }
            }
          },

          cr_lec: {
             validators: {
                notEmpty: {
                message: 'กรอกหน่วยกิต'
              },
                greaterThan: {
                    message: 'ต้อมมีค่ามากกว่า 0',
                     min: 0,
                    }
                }
            },

            cr_lab: {
             validators: {
                notEmpty: {
                message: 'กรอกหน่วยกิต'
              },
                greaterThan: {
                    message: 'ต้อมมีค่ามากกว่า 0',
                     min: 0,
                    }
                }
            },

          credit: {
             validators: {
                notEmpty: {
                message: 'กรอกหน่วยกิต'
              },
                greaterThan: {
                    message: 'ต้อมมีค่ามากกว่า 1',
                     min: 1,
                    }
                }
            },

            detail: {
            validators: {
              notEmpty: {
                message: 'กรอกคำอธิบายรายวิชา'
              }
            }
          },

          group_subj_id: {
            validators: {
              notEmpty: {
                message: 'เลือกกลุ่มวิชา'
              }
            }
          },


        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection

{{-- Extends layout --}}
@extends('layout.default')
<link rel="stylesheet" href="{{ asset('jquery.Thailand.js/dist/jquery.Thailand.min.css') }}">
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-33058582-1', 'auto', {
        'name': 'Main'
    });
    ga('Main.send', 'event', 'jquery.Thailand.js', 'GitHub', 'Visit');
</script>


{{-- Content --}}
@section('content')
    <form class="form" id="kt_form" action="{{ route('teacher.store') }}" method="POST" enctype="multipart/form-data">
        <div class="card card-custom card-sticky" id="kt_page_sticky_card">
            <!--begin::Form-->
            <div class="card-header">
                <div class="card-toolbar">
                    <ul class="nav nav-pills" role="tablist">
                        <li role="presentation" class="active" id="pills-home"><a href="#home" aria-controls="home"
                                                                                  role="tab" data-toggle="pill">ข้อมูลส่วนตัว</a>
                        </li>
                        <li role="presentation" id="pills-profile"><a href="#profile" aria-controls="profile" role="tab"
                                                                      data-toggle="tab">ที่อยู๋</a></li>
                        <li role="presentation" id="pills-study"><a href="#study" aria-controls="study" role="tab"
                                                                    data-toggle="tab">ประวัติการศึกษา</a></li>
                        <li role="presentation" id="pills-messages"><a href="#messages" aria-controls="messages"
                                                                       role="tab" data-toggle="tab">ประวัติการทำงาน</a>
                        </li>
                        <li role="presentation" id="pills-settings"><a href="#settings" aria-controls="settings"
                                                                       role="tab" data-toggle="tab">ครอบครัว</a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body">
                @csrf
                <div>
                    <div class="col-xl-10">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">

                                <div class="my-5">
                                    <div class="form-group row">
                                        <div class="col-6"></div>
                                        <div class="col-6">
                                            <div class="image-input image-input-outline" id="kt_image_2">
                                                <div class="image-input-wrapper"
                                                     style=""></div>

                                                <label
                                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                    data-action="change" data-toggle="tooltip" title=""
                                                    data-original-title="Change Image">
                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                    <input type="file" accept=".png, .jpg, .jpeg" name="t_img"
                                                           id="t_img"/>
                                                    <input type="hidden" name="t_img" id="t_img"/>
                                                    <span class="text-danger form-error"></span>
                                                </label>
                                                <span
                                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow form-error"
                                                    data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                </span>
                                            </div>
                                            <label class="col-9">เพิ่มรูป</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-col-form-label">รหัสอาจารย์</label>
                                        <div class="col-5">
                                            <input class="form-control" type="text" name="t_ID" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class=" col-2 col-form-label">คำนำหน้า</label>
                                        <div class="col-2">
                                            <select class="form-control form-control" name="t_title">
                                                <option value="">คำนำหน้า</option>
                                                <option value="นาย">นาย</option>
                                                <option value="นาย">นาง</option>
                                                <option value="นาย">นางสาว</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ชื่อ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="t_Name_th" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">นามสกุล</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="t_Lname_th" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ชื่อภาษาอังกฤษ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="t_Name_en" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">สกุลภาษาอังกฤษ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" value="" name="t_Lname_en"
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">เลขบัตรประจำตัวประชาชน</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="t_p_ID" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">เบอร์โทร</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="t_tel" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">วันเกิด</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="birthday" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">อีเมล์</label>
                                        <div class="col-4">
                                            <input class="form-control" type="email" name="email" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">

                                        <label class="col-2 col-form-label">ศาสนา</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="religion">
                                                <option value="">เลือกศาสนา</option>
                                                <option value="อิสลาม">อิสลาม</option>
                                                <option value="B">พุทธ</option>
                                                <option value="O">คริสต์</option>
                                                <option value="AB">พราหมณ์-ฮินดู</option>
                                            </select>
                                        </div>
                                        <label class="col-2 col-form-label">กรุ๊ปเลือด</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="blood_type">
                                                <option value="">เลือกกรุ๊ปเลือด</option>
                                                <option value="A">A</option>
                                                <option value="B">B</option>
                                                <option value="O">O</option>
                                                <option value="AB">AB</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">กรุ๊ปเลือด</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="disability">
                                                <option value="">เลือกความพิการ</option>
                                                <option value="ไม่มี">ไม่มี</option>
                                                <option value="ทางการเห็น">ทางการเห็น</option>
                                                <option value="การได้ยิน">การได้ยิน</option>
                                                <option value="ทางร่างกาย">ทางร่างกาย</option>
                                                <option value="ทางการเรียนรู้">ทางการเรียนรู้</option>
                                                <option value="ทางการพูด และภาษา">ทางการพูด และภาษา</option>
                                                <option value="ทางพฤติกรรม หรืออารมณ์">ทางพฤติกรรม หรืออารมณ์</option>
                                                <option value="ออทิสติก">ออทิสติก</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a id="backhome" aria-controls="home" role="tab"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                    </div>
                                </div>
                                <br><br>


                            </div>


                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ที่อยู่ตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="addr_birthplace" value=""
                                                   id="addr_birthplace">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ตำบลตามตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_t" class="form-control" type="text" id="district">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">อำเภอตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_city" class="form-control" type="text" id="amphoe">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">จังหวัดตามบัตร ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_prov" class="form-control" type="text" id="province">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">รหัสไปรษณีย์ตาม ปปช</label>
                                        <div class="col-4">
                                            <input name="addr_b_post" class="form-control" type="text" id="zipcode">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="showChecked"
                                           name="showHideTextbox" value="show">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label class="form-check-label" for="showChecked">ที่อยู่ปัจจุบันเหมือนกันกับที่อยู่ตามบัตรประชาชน</label>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ที่อยู่ปัจจุบัน</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="addr_cerrent" value=""
                                                   id="addr_cerrent">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ตำบล</label>
                                        <div class="col-4">
                                            <input name="addr_c_t" class="form-control" type="text" id="district1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">อำเภอ</label>
                                        <div class="col-4">
                                            <input name="addr_c_city" class="form-control" type="text" id="amphoe1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">จังหวัด</label>
                                        <div class="col-4">
                                            <input name="addr_c_prov" class="form-control" type="text" id="province1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                        <label class="col-2 col-form-label">รหัสไปรษณีย์</label>
                                        <div class="col-4">
                                            <input name="addr_c_post" class="form-control" type="text" id="zipcode1">
                                            <span class="text-danger form-error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a href="#home" aria-controls="home" role="tab"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <a href="#study" aria-controls="study" role="tab"
                                           class="btn btn-primary font-weight-bolder next-tab" data-toggle="tab"> ถัดไป
                                            <i class="ki ki-long-arrow-next"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="study">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">วุฒิการศึกษา</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="degree">
                                                <option value="">ระดับการศึกษา</option>
                                                <option value="ปวช">ปวช</option>
                                                <option value="ปวส">ปวส</option>
                                                <option value="ปริญาตรี">ปริญาตรี</option>
                                                <option value="ปริญาโท">ปริญาโท</option>
                                            </select>
                                        </div>
                                        <label class="col-2 col-form-label">สถาบันที่จบ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="university" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ปีที่สำเร็จการศึกษา</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="year" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a href="#profile" aria-controls="profile" role="tabpanel"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <a href="#messages" aria-controls="messages" role="tabpanel"
                                           class="btn btn-primary font-weight-bolder next-tab" data-toggle="tab"> ถัดไป
                                            <i class="ki ki-long-arrow-next"></i></a>
                                    </div>
                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="messages">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">วันที่เรีมทำงาน</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="work_date" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">วันที่บรรจุ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="date_permit" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">

                                        <label class="col-2 col-form-label">วันที่เกษียณอายุ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="date" name="retire_date" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">ความเชียวชาญ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="expertise" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ตำแหน่งทางวิชาการ</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="position" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">ตำแหน่งบริหาร</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="position_exec" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>


                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">สาขา</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="major_id">
                                                <option value="">เลือกสาขา</option>
                                                <option value="1">วิทยาการคอมพิวเตอร์</option>
                                                <option value="2">เทคโนโลยสารสนเทศ</option>
                                                <option value="3">คอมพิวเตอร์ธุรกิจ</option>
                                            </select>
                                        </div>
                                        <label class="col-2 col-form-label">หมวดวิชาที่สอน</label>
                                        <div class="col-4">
                                            <select class="form-control form-control" name="subj_group_id">
                                                <option value="">หมวดวิชาที่สอน</option>
                                                <option value="1">วิทยาศาสตร์</option>
                                                <option value="2">คณิตศาสตร์</option>
                                                <option value="3">ศึกษาทั่วไป</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a href="#study" aria-controls="study" role="tabpanel"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <a href="#settings" aria-controls="settings" role="tabpanel"
                                           class="btn btn-primary font-weight-bolder next-tab" data-toggle="tab"> ถัดไป
                                            <i class="ki ki-long-arrow-next"></i></a>
                                    </div>
                                </div>
                            </div>


                            <div role="tabpanel" class="tab-pane" id="settings">
                                <div class="my-5">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">ชื่อคู่สมรส</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="mate" value=""
                                                   id="example-text-input">
                                        </div>
                                        <label class="col-2 col-form-label">ชื่อบิดา</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="father" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>

                                <div class="my-5">
                                    <div class="form-group row">

                                        <label class="col-2 col-form-label">ชื่อมารดา</label>
                                        <div class="col-4">
                                            <input class="form-control" type="text" name="mother" value=""
                                                   id="example-text-input">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xl-6"></div>
                                    <div class="col-xl-6">
                                        <a href="#messages" aria-controls="messages" role="tab"
                                           class="btn btn-secondary back-tab" data-toggle="tab"><i
                                                class="ki ki-long-arrow-back"></i> กลับ </a>
                                        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
                                            <i class="ki ki-check icon-sm"></i>
                                            บันทึก
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="separator separator-dashed my-10"></div>

                        </div>

                        <div class="col-xl-2"></div>
                        <!--end::Form-->
    </form>



    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/></link>
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
        var avatar2 = new KTImageInput('kt_image_2');
        $(document).ready(function () {
            $("#showChecked").click(function () {

                if ($(this).is(":checked")) {

                    $("#addr_cerrent").val($("#addr_birthplace").val());
                    $("#district1").val($("#district").val());
                    $("#amphoe1").val($("#amphoe").val());
                    $("#province1").val($("#province").val());
                    $("#zipcode1").val($("#zipcode").val());
                } else {
                }
            });
        });
    </script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.20/js/uikit.min.js"></script>

    <!-- dependencies for zip mode -->
    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/zip.js/zip.js') }}"></script>
    <!-- / dependencies for zip mode -->

    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/JQL.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dependencies/typeahead.bundle.js') }}"></script>

    <script type="text/javascript" src="{{ asset('jquery.Thailand.js/dist/jquery.Thailand.min.js') }}"></script>

    <script type="text/javascript">
        /******************\
         *     DEMO 1     *
         \******************/
        // demo 1: load database from json. if your server is support gzip. we recommended to use this rather than zip.
        // for more info check README.md

        $.Thailand({
            database: '{{ asset('jquery.Thailand.js/database/db.json') }}',


            $district: $('#kt_form [name="addr_b_t"]'),
            $amphoe: $('#kt_form [name="addr_b_city"]'),
            $province: $('#kt_form [name="addr_b_prov"]'),
            $zipcode: $('#kt_form [name="addr_b_post"]'),

            onDataFill: function (data) {
                console.info('Data Filled', data);
            },

            onLoad: function () {
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

        // watch on change

        $('#kt_form [name="addr_b_t"]').change(function () {
            console.log('ตำบล', this.value);
        });
        $('#kt_form [name="addr_b_city"]').change(function () {
            console.log('อำเภอ', this.value);
        });
        $('#kt_form [name="addr_b_prov"]').change(function () {
            console.log('จังหวัด', this.value);
        });
        $('#kt_form [name="addr_b_post"]').change(function () {
            console.log('รหัสไปรษณีย์', this.value);
        });

        $.Thailand({
            database: '{{ asset('jquery.Thailand.js/database/db.json') }}',


            $district: $('#kt_form [name="addr_c_t"]'),
            $amphoe: $('#kt_form [name="addr_c_city"]'),
            $province: $('#kt_form [name="addr_c_prov"]'),
            $zipcode: $('#kt_form [name="addr_c_post"]'),

            onDataFill: function (data) {
                console.info('Data Filled', data);
            },

            onLoad: function () {
                console.info('Autocomplete is ready!');
                $('#loader, .demo').toggle();
            }
        });

    </script>
@endsection

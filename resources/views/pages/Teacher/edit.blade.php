{{-- Extends layout --}}
@extends('layout.default')


{{-- Content --}}
@section('content')

<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        เพิ่มข้อมูลอาจารย์<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('teacher.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        ยกเลิก
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
    <form class="form" id="kt_form" action="{{action('TeacherController@update',$id)}}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-xl-10">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

              <div class="my-5">
                  <div class="form-group row">
                      <div class="col-6"></div>
                      <div class="col-6">
                          <div class="image-input image-input-outline" id="kt_image_2">
                              <div class="image-input-wrapper"
                                   style="">
                                  @if(!empty($teacher->t_img))
                                      <img src="{{ Storage::url($teacher->t_img) }}" alt="" width="100"
                                           height="100">
                                  @endif
                              </div>

                              <label
                                  class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                  data-action="change" data-toggle="tooltip" title=""
                                  data-original-title="Change Image">
                                  <i class="fa fa-pen icon-sm text-muted"></i>
                                  <input type="file" accept=".png, .jpg, .jpeg" name="t_img"
                                         id="t_img"/>
                                  <input type="hidden" name="t_img" id="t_img"/>
                                  <span class="text-danger form-error"></span>
                              </label>
                              <span
                                  class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow form-error"
                                  data-action="cancel" data-toggle="tooltip" title="Cancel Image">
                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                </span>
                          </div>
                          <label class="col-9">เพิ่มรูป</label>
                      </div>
                  </div>
              </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-col-form-label">รหัสอาจารย์</label>
                <div class="col-5">
                <input class="form-control" type="text" name="t_ID" value="{{$teacher->t_ID}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class=" col-2 col-form-label">คำนำหน้า</label>
                <div class="col-2">
                    <select class="form-control form-control" name="t_title">
                        <option value="{{$teacher->t_title}}">{{$teacher->t_title}}</option>
                        <option value="นาย">นาย</option>
                        <option value="นาย">นาง</option>
                        <option value="นาย">นางสาว</option>
                    </select>
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ชื่อ</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="t_Name_th" value="{{$teacher->t_Name_th}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">นามสกุล</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="t_Lname_th" value="{{$teacher->t_Lname_th}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ชื่อภาษาอังกฤษ</label>
                <div class="col-4">
                <input class="form-control" type="text" name="t_Name_en"  value="{{$teacher->t_Name_en}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">สกุลภาษาอังกฤษ</label>
                <div class="col-4">
                <input class="form-control" type="text" name="t_Lname_en" value="{{$teacher->t_Lname_en}}"   id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">เลขบัตรประจำตัวประชาชน</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="t_p_ID" value="{{$teacher->t_p_ID}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">เบอร์โทร</label>
                <div class="col-4">
                <input class="form-control" type="text" name="t_tel" value="{{$teacher->t_tel}}" id="example-text-input">
                </div>
            </div>
          </div>



          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">อีเมล์</label>
                <div class="col-4">
                <input class="form-control" type="email" name="email"  value="{{$teacher->email}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ศาสนา</label>
                <div class="col-4">
                    <select class="form-control form-control" name="religion">
                        <option value="{{$teacher->religion}}">{{$teacher->religion}}</option>
                        <option value="อิสลาม">อิสลาม</option>
                        <option value="B">พุทธ</option>
                        <option value="O">คริสต์</option>
                        <option value="AB">พราหมณ์-ฮินดู </option>
                    </select>
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">กรุ๊ปเลือด</label>
                <div class="col-4">
                    <select class="form-control form-control" name="blood_type">
                        <option value="{{$teacher->blood_type}}">{{$teacher->blood_type}}</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
                    </select>
                </div>
                <label class="col-2 col-form-label">ความพิการ</label>
                <div class="col-4">
                    <select class="form-control form-control" name="disability">
                        <option value="{{$teacher->disability}}">{{$teacher->disability}}</option>
                        <option value="">เลือกความพิการ</option>
                        <option value="ไม่มี">ไม่มี</option>
                        <option value="ทางการเห็น">ทางการเห็น</option>
                        <option value="การได้ยิน">การได้ยิน</option>
                        <option value="ทางร่างกาย">ทางร่างกาย</option>
                        <option value="ทางการเรียนรู้">ทางการเรียนรู้</option>
                        <option value="ทางการพูด และภาษา">ทางการพูด และภาษา</option>
                        <option value="ทางพฤติกรรม หรืออารมณ์">ทางพฤติกรรม หรืออารมณ์</option>
                        <option value="ออทิสติก">ออทิสติก</option>
                    </select>
                </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ที่อยู่ตามบัตร ปปช</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="addr_birthplace" value="{{$teacher->addr_birthplace}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ตำบลตามบัตรประจำตัวประชาชน</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="addr_b_t" value="{{$teacher->district}}" id="example-text-input">
                </div>

            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">อำเภอตามบัตร ปปช</label>
                <div class="col-4">
                <input class="form-control" type="text"  name="addr_b_city" value="{{$teacher->amphoe}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">จังหวัดตามบัตรประจำตัวประชาชน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_b_prov"  value="{{$teacher->province}}" id="example-text-input">
                </div>
            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">รหัสไปรษณีย์ ปปช</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_b_post"  value="{{$teacher->zipcode}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ที่อยู่ปัจจุบัน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_cerrent"  value="{{$teacher->addr_cerrent}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ตำบลปัจจุบัน</label>
                <div class="col-4">
                <input class="form-control" type="text "name="addr_c_t" value="{{$teacher->district1}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">อำเภอปัจจุบัน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_c_city" value="{{$teacher->amphoe1}}" id="example-text-input">
                </div>
            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">จังหวัดปัจจุบัน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_c_prov" value="{{$teacher->province1}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">รหัสไปรษณีย์ปัจจุบัน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="addr_c_post" value="{{$teacher->zipcode1}}" id="example-text-input">
                </div>
            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">วันเกิด</label>
                <div class="col-4">
                <input class="form-control" type="date" name="birthday" value="{{$teacher->birthday}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">วันที่เรีมทำงาน</label>
                <div class="col-4">
                <input class="form-control" type="date" name="work_date" value="{{$teacher->work_date}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">วันที่บรรจุ</label>
                <div class="col-4">
                <input class="form-control" type="date" name="date_permit" value="{{$teacher->date_permit}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">วันที่เกษียณอายุ</label>
                <div class="col-4">
                <input class="form-control" type="date" name="retire_date" value="{{$teacher->retire_date}}" id="example-text-input">
                </div>
            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ความเชียวชาญ</label>
                <div class="col-4">
                <input class="form-control" type="text" name="expertise" value="{{$teacher->expertise}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">สาขา</label>
                <div class="col-4">
                <input class="form-control" type="text" name="major_id" value="{{$teacher->major_id}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">หมวดวิชาที่สอน</label>
                <div class="col-4">
                <input class="form-control" type="text" name="subj_group_id" value="{{$teacher->subj_group_id}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ชื่อคู่สมรส</label>
                <div class="col-4">
                <input class="form-control" type="text" name="mate" value="{{$teacher->mate}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ชื่อบิดา</label>
                <div class="col-4">
                <input class="form-control" type="text" name="father" value="{{$teacher->father}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ชื่อมารดา</label>
                <div class="col-4">
                <input class="form-control" type="text" name="mother" value="{{$teacher->mother}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ตำแหน่งทางวิชาการ</label>
                <div class="col-4">
                <input class="form-control" type="text" name="position" value="{{$teacher->position}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">ตำแหน่งบริหาร</label>
                <div class="col-4">
                <input class="form-control" type="text" name="position_exec" value="{{$teacher->position_exec}}" id="example-text-input">
                </div>
            </div>
          </div>


          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">วุฒิการศึกษา</label>
                <div class="col-4">
                <input class="form-control" type="text" name="degree" value="{{$teacher->degree}}" id="example-text-input">
                </div>
                <label class="col-2 col-form-label">สถาบันที่จบ</label>
                <div class="col-4">
                <input class="form-control" type="text" name="university" value="{{$teacher->university}}" id="example-text-input">
                </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-2 col-form-label">ปีที่สำเร็จการศึกษา</label>
                <div class="col-4">
                <input class="form-control" type="text" name="year" value="{{$teacher->year}}" id="example-text-input">
                </div>
            </div>
          </div>


          <input type="hidden" name="_method" value="PATCH"/>

          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('.select2').select2();
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      $('#kt_form').submit();
    });
  });
  var avatar1 = new KTImageInput('kt_image_1');
</script>
@endsection

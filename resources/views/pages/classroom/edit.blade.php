{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{action('ClassroomController@update',$id)}}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        แก้ใขห้องเรียน<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('classroom.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">ห้องเรียน :</h3>
            <div class="form-group row">
                <label class="col-3">รหัสห้องเรียน</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="clr_ID" type="text" value="{{$classroom->clr_ID}}" />
                </div>
              </div>
            <div class="form-group row">
              <label class="col-3">ชื่อห้องเรียน</label>
              <div class="col-9">
                <input class="form-control form-control-solid" name="clr_Name" type="text" value="{{$classroom->clr_Name}}" />
              </div>
            </div>
          </div>

          <div class="my-5">
            <div class="form-group row">
                <label class="col-3">ประเภท</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="clr_Type" type="text" value="{{$classroom->clr_Type}}" />
                </div>
              </div>
            <div class="form-group row">
              <label class="col-3">จำนวนที่นั่ง</label>
              <div class="col-9">
                <input class="form-control form-control-solid" id="clr_Seat" name="clr_Seat" type="number" value="{{$classroom->clr_Seat}}" />
              </div>
            </div>
          </div>

          <input type="hidden" name="_method" value="PATCH"/>
          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            mc_id: {
            validators: {
              notEmpty: {
                message: 'กรอกรหัสหลักสูตร'
              }
            }
          },

          mc_name: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อหลักสูตร'
              }
            }
          },

          mc_year: {
            validators: {
              notEmpty: {
                message: 'เลือกปีที่อนุมัติหลักสูตร'
              }
            }
          },


        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection

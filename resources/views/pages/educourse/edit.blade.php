{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{action('EducourseController@update',$id)}}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        แก้ใข<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('educourse.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">สาขาวิชา :</h3>
            <div class="form-group row">
                <label class="col-3">คณะ/วิทยาลัย</label>
                <div class="col-9">
                    <select class="form-control form-control" name="fac_id">
                        <option value="{{$edu->fac_id}}">{{$edu->faculty->fac_name}}</option>
                        <option value="">เลือกคณะ</option>
                        @foreach($faculty as $row )
                            <option value="{{$row->fac_id}}">{{$row->fac_name}}</option>
                        @endforeach
                    </select>
                </div>
              </div>
            <div class="form-group row">
                <label class="col-3">สาขา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="major_id">
                        <option value="{{$edu->major_id}}">{{$edu->major->major_name}}</option>
                        @foreach($major as $row )
                                    <option value="{{$row->major_id }}">{{$row->major_name}}</option>
                        @endforeach
                   </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">หลักสูตร</label>
                <div class="col-9">
                    <select class="form-control form-control" name="mc_id">
                        <option value="{{$edu->mc_id}}">{{$edu->maincourse->mc_name}}</option>
                        @foreach($mainCourse as $row )
                                    <option value="{{$row->mc_id  }}">{{$row->mc_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">ปีการศึกษา</label>
                <div class="col-9">
                    <select name="mc_year" id="year" class="form-control">
                        <option value="{{$edu->mc_year}}">{{$edu->mc_year}}</option>
                        <?php for($i=0; $i<=20; $i++) { ?>
                        <option value="<?php echo date("Y")-$i+543; ?>"><?php echo date("Y")-$i+543; ?></option>
                        <?php } ?>
                        </select>
                </div>
         </div>

            <div class="form-group row">
                <label class="col-3">ชั้นปึ</label>
                <div class="col-9">
                    <select class="form-control form-control" name="academic_y">
                        <option value="{{$edu->academic_y}}">{{$edu->academic_y}}</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">ภาคการศึกษา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="term">
                        <option value="{{$edu->term}}">{{$edu->term}}</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">วิชา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="subj_id">
                        <option value="{{$edu->subj_id}}">{{$edu->subject->subj_name_th}}</option>
                        @foreach($subject as $row )
                                    <option value="{{$row->subj_id}}">{{$row->subj_name_th}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">กลุ่มวิชา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="group_subj_id">
                        <option value="{{$edu->group_subj_id}}">{{$edu->groupSubj->group_subj_name}}</option>
                        <option value="">เลือกกลุ่มวิชา</option>
                        @foreach($groupSubj as $row )
                                    <option value="{{$row->group_subj_id }}">{{$row->group_subj_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-3">หมวดวิชา</label>
                <div class="col-9">
                    <select class="form-control form-control" name="subj_group_id">
                        <option value="{{$edu->subj_group_id}}">{{$edu->SubjGroup->subj_group_name}}</option>
                        @foreach($subjGroupe as $row )
                                    <option value="{{$row->subj_group_id }}">{{$row->subj_group_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
          <input type="hidden" name="_method" value="PATCH"/>
          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            group_subj_id: {
            validators: {
              notEmpty: {
                message: 'กรอกรหัสสาขา'
              },
               integer: {
                         message: 'The value is not an integer',
              }
            }
          },

          group_subj_name: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อสาขา'
              }
            }
          },

          subj_group_id: {
            validators: {
              notEmpty: {
                message: 'กรุณาเลือกหมวดวิชา'
              }
            }
          },


        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection

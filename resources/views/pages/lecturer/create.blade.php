{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{ route('lecturer.store') }}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        กำหนดผู้สอน<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('lecturer.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          เพิ่มข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">กำหนดผู้สอน :</h3>

              <div class="form-group row">
                    <label class="col-3">อาจารย์</label>
                    <div class="col-9">
                        <select class="form-control form-control" name="t_ID">
                            <option value="">เลือกอาจารย์ผู้สอน</option>
                            @foreach($teache as $row )
                                <option value="{{$row->t_ID}}">{{$row->t_Name_th}}  {{$row->t_Lname_th}}</option>
                            @endforeach
                        </select>
                    </div>
               </div>

               <div class="form-group row">
                    <label class="col-3">วิชา</label>
                    <div class="col-9">
                        <select class="form-control form-control" name="subj_id">
                            <option value="">เลือกวิชา</option>
                            @foreach($subject as $row )
                                <option value="{{$row->subj_id}}">{{$row->subj_name_th}}</option>
                            @endforeach
                        </select>
                    </div>
              </div>

              <div class="form-group row">
                <label class="col-3">กลุ่ม</label>
                <div class="col-9">
                    <input class="form-control form-control-solid" id="session" name="session" type="number" value="" />
                </div>
              </div>

              <div class="form-group row">
                    <label class="col-3">ภาคเรียน</label>
                    <div class="col-9">
                        <select class="form-control form-control" name="term">
                            <option value="">เลือกภาคเรียน</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                    </div>
              </div>

              <div class="form-group row">
                    <label class="col-3">ปีการศึกษา</label>
                    <div class="col-9">
                        <select name="year" id="year" class="form-control">
                            <option value="">เลือกปี:</option>
                            <?php for($i=0; $i<=20; $i++) { ?>
                            <option value="<?php echo date("Y")-$i+543; ?>"><?php echo date("Y")-$i+543; ?></option>
                            <?php } ?>
                            </select>
                    </div>
             </div>

             <div class="form-group row">
                <label class="col-3">จำนวน</label>
                <div class="col-9">
                    <input class="form-control form-control-solid" id="seat" name="seat" type="number" value="" />
                </div>
              </div>


            <div class="form-group row">
            </div>
         </div>


          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>
</div>

@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            t_ID: {
            validators: {
              notEmpty: {
                message: 'เลือกอาจารย์'
              },
            }
          },

          subj_id: {
            validators: {
              notEmpty: {
                message: 'เลือกวิชา'
              }
            }
          },

          session: {
            validators: {
              notEmpty: {
                message: 'กรอกกลุ่ม'
              }
            }
          },

          term: {
             validators: {
                 notEmpty: {
                 message: 'เลือกภาคเรียน'
                    }
               }
            },

            year: {
             validators: {
                 notEmpty: {
                 message: 'เลือกปีการศึกษา'
                    }
               }
            },

            seat: {
             validators: {
                notEmpty: {
                message: 'จำนวนที่นั่ง'
              },
                greaterThan: {
                    message: 'ต้อมมีค่ามากกว่า 1',
                     min: 1,
                    }
                }
            },
        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection


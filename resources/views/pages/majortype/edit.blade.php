{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<form class="form" id="kt_form" action="{{action('MajortypeController@update',$id)}}" method="POST" enctype="multipart/form-data">
<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <!--begin::Form-->
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        แก้ใขหมวดวิชา<i class="mr-2"></i>
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{route('majortype.index')}}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        กลับ
      </a>
      <div class="btn-group">
        <button type="submit" id="save_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          แก้ใขข้อมูล
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          @if ($errors->any())
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="my-5">
            <h3 class=" text-dark font-weight-bold mb-10">แก้ใขประเภทวิชา :</h3>
            <div class="form-group row">
                <label class="col-3">รหัสประเภทวิชา</label>
                <div class="col-9">
                  <input class="form-control form-control-solid" name="major_type_id" type="text" value="{{$majorType->major_type_id}}" />
                </div>
              </div>
            <div class="form-group row">
              <label class="col-3">ชื่อประเภทวิชา</label>
              <div class="col-9">
                <input class="form-control form-control-solid" name="major_type_name" type="text" value="{{$majorType->major_type_name}}" />
              </div>
            </div>
          </div>

          <input type="hidden" name="_method" value="PATCH"/>
          <div class="separator separator-dashed my-10"></div>

        </div>
        <div class="col-xl-2"></div>
        <!--end::Form-->
    </form>
  </div>


</div>

</div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
  $(document).ready(function() {
    $('#save_form').on('click', function() {
      // window.location = "/purchase-order";
      // $('#kt_form').submit();
    });

    FormValidation.formValidation(
      document.getElementById('kt_form'), {
        fields: {
            major_type_id: {
            validators: {
              notEmpty: {
                message: 'กรอกประเภทวิชา'
              }
            }
          },

          major_type_name: {
            validators: {
              notEmpty: {
                message: 'กรอกชื่อประเภทวิชา'
              }
            }
          },


        },

        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap(),
          // Validate fields when clicking the Submit button
          submitButton: new FormValidation.plugins.SubmitButton(),
          // Submit the form when all fields are valid
          defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
        }
      }
    );
  });
</script>
@endsection


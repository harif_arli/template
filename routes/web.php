<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'PagesController@index')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('student', 'StudentController');
    Route::resource('teacher', 'TeacherController');
    Route::resource('employee', 'EmployeeController');
    Route::resource('major', 'MajorController');
    Route::resource('subjectgroup', 'SubjectgroupController', ['names' => 'subjectgroup']);
    Route::resource('groupsubject', 'GroupsubjectController');
    Route::resource('subject', 'SubjectController');
    Route::resource('majortype', 'MajortypeController');
    Route::resource('faculty', 'FacultyController');
    Route::resource('course', 'CourseController');
    Route::resource('classroom', 'ClassroomController');
    Route::resource('lecturer', 'LecturerController');
    Route::resource('educourse', 'EducourseController');

    Route::post('/find/district', 'ProviceController@district')->name('get_district');
    Route::post('/find/subdistrict', 'ProviceController@subdistrict')->name('get_subdistrict');
    Route::post('/find/poscode', 'ProviceController@poscode')->name('get_poscode');
    Route::post('/find/district1', 'ProviceController@district1')->name('get_district1');
    Route::post('/find/subdistrict1', 'ProviceController@subdistrict1')->name('get_subdistrict1');

//  Route::resource('purchase-order', 'PurchaseOrderController');
    //   Route::resource('department', 'DepartmentController');
    //   Route::resource('branch', 'BranchController');
    //   Route::resource('company', 'CompanyController');
});

// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');

Auth::routes();
Route::get('/home', 'PagesController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');
